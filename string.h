#include <iostream>
#include <cstring>
#pragma GCC optimize ("O3")

class String {
private:
    char*  stringHead;
    size_t stringLength;
    size_t bufferSize;

    void growBuffer();
    void shrinkBuffer();
    bool compare(const String& otherString, const size_t idx) const;
    void copy(const size_t pos, const char* otherString, const size_t length);
public:
    String();
    ~String();
    String(const char* cString);
    String(const String& other);
    String(const int n, const char symbol);

    String& operator=(const String& otherString);
    String& operator+=(const String& otherString);
    String& operator+=(const char symbol);

    bool operator==(const String& otherString)  const;
    char& operator[](const int index);
    char operator[](const int index) const;

    size_t length() const;

    void push_back(const char& symbol);
    void pop_back();
    char& front();
    char front() const;

    char& back();
    char back() const;

    unsigned int find(const String& subString) const;
    unsigned int rfind(const String& subString) const;
    String substr(const int start, const int count) const;

    bool empty() const;
    void clear();
};

String::String()
{
    bufferSize = 100ul;
    stringLength = 0ul;
    stringHead = (char*) malloc(bufferSize);
}

String::~String()
{
    free(stringHead);
}

String::String(const char * cString)
{
    if(cString == nullptr)
    {
        stringLength = 0ul;
        bufferSize = 100ul;
        stringHead = (char*) malloc(bufferSize);
    }
    else
    {
        stringLength = strlen(cString);
        bufferSize = stringLength + stringLength;
        stringHead = (char*) malloc(bufferSize);
        copy(0ul, cString, stringLength);
    }
}

String::String(const String &other)
{
    stringLength = other.stringLength;
    bufferSize = other.bufferSize;
    stringHead = (char*) malloc(bufferSize);
    copy(0ul, other.stringHead, stringLength);
}

String::String(const int n, const char symbol)
{
    bufferSize = n + n;
    stringLength = n;
    stringHead = (char*) malloc(bufferSize);
    for (size_t i = 0; i < stringLength; ++i)
    {
        stringHead[i] = symbol;
    }
}

inline String& String::operator=(const String &otherString)
{
    if (this != &otherString)
    {
        free(stringHead);
        bufferSize = otherString.bufferSize;
        stringLength = otherString.stringLength;
        stringHead = (char*) malloc(bufferSize);
        copy(0ul, otherString.stringHead, stringLength);
    }
    return *this;
}

inline String& String::operator+=(const String &otherString)
{
    if (otherString.stringLength == 0ul)
        return *this;
    bufferSize += otherString.bufferSize;
    stringHead = (char*) realloc(stringHead, bufferSize);

    copy(stringLength, otherString.stringHead, otherString.stringLength);
    stringLength += otherString.stringLength;
    return *this;
}

inline String& String::operator+=(const char symbol)
{
    push_back(symbol);
    return *this;
}

inline String operator+(const String &leftString, const String &rightString)
{
    String result(leftString);
    result += rightString;
    return result;
}

inline String operator+(const String &leftString, const char rightSymbol)
{
    String result(leftString);
    result += rightSymbol;
    return result;
}

inline String operator+(const char leftSymbol, const String &rightString)
{
    String result(1, leftSymbol);
    result += rightString;
    return result;
}


inline bool String::operator==(const String &otherString) const
{
    bool result = stringLength == otherString.stringLength;
    size_t it = 0;
    while (it < stringLength && result)
    {
        result = stringHead[it] == otherString.stringHead[it];
        ++it;
    }
    return result;
}

inline char& String::operator[](const int index)
{
    return stringHead[index];
}

inline char String::operator[](const int index) const
{
    return stringHead[index];
}

inline size_t String::length() const
{
    return stringLength;

}

inline void String::push_back(const char& symbol)
{
    if (stringLength == bufferSize)
    {
        growBuffer();
    }
    stringHead[stringLength++] = symbol;
}

inline void String::pop_back()
{
    --stringLength;
    if (stringLength * 4ul <= bufferSize)
    {
        shrinkBuffer();
    }
}

inline char& String::front()
{
    return *stringHead;
}

inline char String::front() const
{
    return *stringHead;
}

inline char& String::back()
{
    return *(stringHead + stringLength - 1);
}

inline char String::back() const
{
    return *(stringHead + stringLength - 1);
}

inline unsigned int String::find(const String &subString) const
{
    for (size_t i = 0; i < stringLength - subString.stringLength; ++i)
    {
        if (compare(subString, i))
            return (unsigned int)i;
    }
    return (unsigned int)stringLength;
}

inline unsigned int String::rfind(const String &subString) const
{
    for (int i = stringLength - subString.stringLength - 1; i >= 0; --i)
    {
        if (compare(subString, i))
            return (unsigned int)i;
    }
    return (unsigned int)stringLength;
}

inline String String::substr(const int start, const int count) const
{
    String result;
    result.stringLength = count;
    result.bufferSize = result.stringLength;
    result.stringHead = (char*) malloc(result.bufferSize);

    for (int i  = 0; i < count; ++i)
    {
        result.stringHead[i] = stringHead[start + i];
    }
    return result;
}

inline bool String::empty() const
{
    return stringLength == 0;
}

inline void String::clear()
{
    free(stringHead);
    bufferSize = 100ul;
    stringLength = 0;
    stringHead = (char*) malloc(bufferSize);
}

inline void String::growBuffer()
{
    bufferSize *= 2ul;
    char* newBody = (char*) malloc(bufferSize);
    size_t i;
    for (i = 0ul; i < stringLength - 3; i += 4)
    {
        newBody[i] = stringHead[i];
        newBody[i + 1] = stringHead[i + 1];
        newBody[i + 2] = stringHead[i + 2];
        newBody[i + 3] = stringHead[i + 3];
    }
    for (; i < stringLength; ++i)
        newBody[i] = stringHead[i];
    free(stringHead);
    stringHead = newBody;
}

inline void String::shrinkBuffer()
{
    bufferSize /= 2ul;
    char* newBody = (char*) malloc(bufferSize);
    size_t i;
    for (i = 0ul; i < stringLength - 3; i += 4)
    {
        newBody[i] = stringHead[i];
        newBody[i + 1] = stringHead[i + 1];
        newBody[i + 2] = stringHead[i + 2];
        newBody[i + 3] = stringHead[i + 3];
    }
    for (; i < stringLength; ++i)
        newBody[i] = stringHead[i];
    free(stringHead);
    stringHead = newBody;
}

inline bool String::compare(const String &otherString, const size_t idx) const
{
    bool compareIndicator = true;
    size_t it = 0;
    while (compareIndicator && it < otherString.stringLength)
    {
        compareIndicator = stringHead[idx + it] == otherString.stringHead[it];
        ++it;
    }
    return compareIndicator;
}

inline void String::copy(const size_t pos, const char * otherString, const size_t length)
{
    for (size_t i = 0ul; i < length; ++i)
    {
        stringHead[i + pos] = otherString[i];
    }
}

std::istream& operator>> (std::istream &in, String& string)
{
    string.clear();
    char symb;

    while (true)
    {
        symb = in.get();
        if (symb == '\n' || symb == ' ' || symb == '\0' || symb == EOF)
            break;
        string.push_back(symb);
    }
    return in;
}

std::ostream& operator<< (std::ostream &out, const String& string)
{
    for (size_t i = 0; i < string.length(); ++i)
    {
        out << string[i];
    }
    return out;
} 
