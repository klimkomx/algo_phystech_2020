#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::pair;
using std::cin;
using std::cout;

class dsu {
    vector<int> prev;
    vector<int> s_size;
public:
    dsu(int n) {
        prev.resize(n);
        for (int i = 0; i < n; ++i)
            prev[i] = i;
        s_size.resize(n, 1);
    }
    dsu() = delete;
    ~dsu() = default;
    void unite(int ft, int sd) {
        ft = get_prev(ft);
        sd = get_prev(sd);
        if (ft == sd)
            return;
        if (s_size[ft] < s_size[sd]) {
            prev[ft] = sd;
            s_size[sd] += s_size[ft];
        } else {
            prev[sd] = ft;
            s_size[ft] += s_size[sd];
        }
    }
    int get_prev(int vt) {
        while (vt != prev[vt])
            vt = prev[vt];
        return prev[vt] = vt;
    }
    bool same_set(int ft, int sd) {
        return get_prev(ft) == get_prev(sd);
    }
};

long cruscal(vector<pair<int, pair<int, int> > >& graph,
             int n, int m) {
    std::sort(graph.begin(), graph.end());
    long weight = 0;
    auto d_set = dsu(n);
    for (int i = 0; i < m; ++i) {
        if (!d_set.same_set(graph[i].second.first,
                            graph[i].second.second)) {
            weight += graph[i].first;
            d_set.unite(graph[i].second.first,
                        graph[i].second.second);
        }
    }
    return weight;
}

int main() {
    std::ios::sync_with_stdio(0);
    cin.tie(0);

    int n, m, a, b, c;

    cin >> n >> m;

    vector<pair<int, pair<int, int> > > g;
    for (int i = 0; i < m; ++i) {
        cin >> a >> b >> c;
        --a, --b;
        g.push_back({c, {a, b}});
    }

    cout << cruscal(g, n, m);
    
    return 0;
}