#include <iostream>
#include <algorithm>
#include <queue>

class node {
public:
    int x, y, siz;
    node *l, *r;
    node(int);
};

class cartesianTree {
private:
    node* root;
    static int gets(const node* r);
    static node* merge(node* r1, node* r2);
    static std::pair<node*, node*> split(node* r, int k);
    static int getMax(int num, node* r);
public:
    cartesianTree();
    ~cartesianTree();
    cartesianTree(const cartesianTree& other) = delete;
    void insert(int k);
    void erase(int k);
    int getOrederedElement(int num);
};

node::node (int val) {
    siz = 1ll;
    x = val;
    y = (rand()<<15)+rand();
    l = r = nullptr;
}

int cartesianTree::gets(const node* r) {
    if (r == nullptr)
        return 0ll;
    return r->siz;
}

node* cartesianTree::merge(node* r1, node* r2) {
    if (r1==nullptr) return r2;
    if (r2==nullptr) return r1;
    if (r1 -> x > r2 -> x) std::swap(r1, r2);
    if (r1->y < r2->y) {
        r1->r = merge(r1->r, r2);
        r1->siz = 1 + gets(r1->r) + gets(r1->l);
        return r1;
    }
    else {
        r2->l = merge(r1, r2->l);
        r2->siz = 1 + gets(r2->l) + gets(r2->r);
        return r2;
    }
}

std::pair<node*, node*> cartesianTree::split(node* r, int k) {
    if (r == nullptr)
        return {nullptr, nullptr};
    if (k > r -> x) {
        std::pair<node*, node*> t = split(r -> r, k);
        r -> r = t.first;
        r -> siz = 1 + gets(r->l) + gets(r->r);
        return {r, t.second};
    } else {
        std::pair<node*, node*> t = split(r -> l, k);
        r -> l = t.second;
        r -> siz = 1 + gets(r->l) + gets(r->r);
        return {t.first, r};
    }
}

int cartesianTree::getMax(int num, node* r) {
    while (num != gets(r -> r) + 1) {
        if (gets(r -> r) >= num)
            r = r -> r;
        else {
            num -= gets(r -> r) + 1;
            r = r -> l;
        }
    }
    return r ->x;
}

cartesianTree::cartesianTree():
root(nullptr)
{}

cartesianTree::~cartesianTree() {
	std::queue<node*> nodes;
    node* curNode;
    if (root != nullptr)
        nodes.push(root);
    while (!nodes.empty()) {
        curNode = nodes.front();
        nodes.pop();
        if (curNode -> l != nullptr)
            nodes.push(curNode -> l);
        if (curNode -> r != nullptr)
            nodes.push(curNode -> r);
        delete curNode;
    }
}

void cartesianTree::insert(int k) {
    node* no  = new node(k);
    std::pair<node*, node*> q = split(root, k);
    q.first = merge(q.first, no);
    root = merge(q.first, q.second);
}

void cartesianTree::erase(int k) {
    std::pair<node*, node*> q = split(root, k);
    std::pair<node*, node*> w = split(q.second, k + 1);
    if (w.first != nullptr)
        delete w.first;
    root =  merge(q.first, w.second);
}

int cartesianTree::getOrederedElement(int num) {
    return getMax(num, root);
}


int32_t main() {
    int n, a, b, com;
    cartesianTree tree;
    std::cin >> n;

    for (int i = 0; i < n; ++i) {
        std::cin >> com >> a;

        if (com == 1) {
            tree.insert(a);
        } else if (com == -1) {
            tree.erase(a);
        } else {
            std::cout << tree.getOrederedElement(a) << "\n";
        }
    }
    return 0;
} 
