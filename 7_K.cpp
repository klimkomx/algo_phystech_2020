#include <iostream>
#include <vector>
#include <string>

inline void input_optimization() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(false);
}

bool isEdge(int x, int y, int n) {
    if (n == 1)
        return 1;
    for (int i = 0; i < n - 1; ++i) {
        if ((((x >> i) & 3)==0 && ((y >> i) & 3)==0) || (((x >> i) & 3)==3 && ((y >> i) & 3)==3))
            return 0;
    }
    return 1;
}

int cute_patterns(int n, int m) {
    int n_p = (1 << n);
    int edge[n_p][n_p];
    int dp[n_p][m];
    for (int i = 0; i < n_p; ++i) {
        for (int j = 0; j < n_p; ++j) {
            edge[i][j] = isEdge(i, j, n);
        }
    }
    for (int i = 0; i < n_p; ++i) {
        dp[i][0] = 1;
    }
    for (int i = 1; i < m; ++i) {
        for (int j = 0; j < n_p; ++j) {
            dp[j][i] = 0;
            for (int k = 0; k < n_p; ++k)
                if (edge[k][j])
                    dp[j][i] += dp[k][i - 1];
        }
    }
    int ans = 0;
    for (int i = 0; i < n_p; ++i) {
        ans += dp[i][m - 1];
    }
    return ans;
}

int32_t main() {
    input_optimization();
    int n, m;

    std::cin >> n >> m;

    if (n > m)
        std::swap(n, m);

    std::cout << cute_patterns(n, m);
    return 0;
}
