#include <iostream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>
#include <deque>
#include <map>

class Vertex {
public:
    unsigned long long self;

    struct point {
        unsigned int x, y;
        point(short int x, short int y): x(x), y(y) {}
    };

    Vertex() = default;
    ~Vertex() = default;
    Vertex(const Vertex&) = default;

    point find_pos(int num) const {
        for(int i = 0; i < 4; ++i)  {
            for (int j = 0; j < 4; ++j) {
                if (get_num(point(i, j)) == num) {
                    return point(i, j);
                }
            }
        }
    }

    void set(unsigned long long num, const point& pos) {
        self = (self ^ (self & (15ull << (4*(4 * pos.x + pos.y))))) | (num << (4 * (4 * pos.x + pos.y)));
    }

    int get_num(const point& pos) const {
        return static_cast<int>(((self >> static_cast<unsigned long long>((4 * (4 * pos.x + pos.y)))) & 15ull));
    }

    Vertex next_right() const {
        point npos = find_pos(0);
        point pt(npos.x, npos.y - 1);
        auto ans(*this);
        ans.set(get_num(pt), npos);
        ans.set(0, pt);
        return ans;
    }
    Vertex next_left() const {
        point npos = find_pos(0);
        point pt(npos.x, npos.y + 1);
        auto ans(*this);
        ans.set(get_num(pt), npos);
        ans.set(0, pt);
        return ans;
    }
    Vertex next_up() const {
        point npos = find_pos(0);
        point pt(npos.x - 1, npos.y);
        auto ans(*this);
        ans.set(get_num(pt), npos);
        ans.set(0, pt);
        return ans;
    }
    Vertex next_down() const {
        point npos = find_pos(0);
        point pt(npos.x + 1, npos.y);
        auto ans(*this);
        ans.set(get_num(pt), npos);
        ans.set(0, pt);
        return ans;
    }

    int heuristics() const {
        int ans = 0;
        int num;
        for (int i = 0; i < 4; ++i)
            for (int j = 0; j < 4; ++j) {
                num = get_num(point(i, j));
                if (num == 0)
                    ans += std::abs(3 - i) + std::abs(3 - j);
                else
                    ans += std::abs((num - 1) / 4 - i) + std::abs((num - 1) % 4 - j);
            }
        return ans;
    }
    std::vector<Vertex> nextStep() const {
        std::vector<Vertex> ans;
        auto np = find_pos(0);
        if (np.x % 4 != 0)
            ans.push_back(next_up());
        if (np.x != 3)
            ans.push_back(next_down());
        if (np.y != 0)
            ans.push_back(next_right());
        if (np.y != 3)
            ans.push_back(next_left());
        return ans;
    }

    bool solvable() {
        int r = 0, num1, num2;
        for (int i = 0; i < 16; ++i) {
            num1 = get_num(point(i / 4, i % 4));
            if (num1 == 0)
                r += i / 4;
            else
                for (int j = 0; j < i; ++j) {
                    num2 = get_num(point(j / 4, j % 4));
                    if (num2 != 0 && num1 < num2)
                        ++r;
                }
        }
        return r % 2 == 1;
    }

    bool operator==(const Vertex& oth) const {
        return self == oth.self;
    }
    bool operator!=(const Vertex& oth) const {
        return self != oth.self;
    }
    bool operator<(const Vertex& oth) const {
        return self < oth.self;
    }
    bool operator>(const Vertex& oth) const {
        return self > oth.self;
    }
    void print() const {
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j)
                std::cout << get_num(point(i, j)) << " ";
        }
        std::cout << std::endl;
    }
};

void a_star(std::map<unsigned long long, int>& dist, std::map<unsigned long long, Vertex>& pr,
            std::map<unsigned long long, bool>& used, Vertex& s, Vertex& t) {
    if (!s.solvable())
        return;
    used[s.self] = true;
    dist[s.self] = 0;
    std::priority_queue<std::pair<int, Vertex> > q;
    q.push({-s.heuristics(), s});
    while (!q.empty()) {
        auto ev = -q.top().first;
        auto vt = q.top().second;
//        vt.print();
//        std::cout << vt.heuristics() + dist[vt.self] << std::endl;
        q.pop();
        if (ev != vt.heuristics())
            continue;
        if (vt == t)
            return;
        auto dt = dist[vt.self] + 1;
        for (auto v : vt.nextStep()) {
            if (!used[v.self] || dist[v.self] > dt) {
                used[v.self] = true;
                dist[v.self] = dt;
                pr[v.self] = vt;
                q.push({ - v.heuristics(), v});
            }
        }
    }
}
int main() {
    int a;
    Vertex b, e;
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            std::cin >> a;
            b.set(a, Vertex::point(i, j));
            e.set((4 * i + j + 1) % 16, Vertex::point(i, j));
//            b.print();
//            e.print();
        }
    }
    std::map<unsigned long long, int> dist;
    std::map<unsigned long long, Vertex> pr;
    std::map<unsigned long long, bool> used;
    a_star(dist, pr, used, b, e);

    if (!used[e.self]) {
        std::cout << -1;
        return 0;
    }

    std::cout << dist[e.self] << std::endl;
    std::string ans;
    while(e != b) {
        auto e_prev = pr[e.self];
        auto e_n = e.find_pos(0),
             e_prev_n = e_prev.find_pos(0);
        if (e_n.x > e_prev_n.x)
            ans.push_back('U');
        else if (e_n.x < e_prev_n.x)
            ans.push_back('D');
        if (e_n.y > e_prev_n.y)
            ans.push_back('L');
        if (e_n.y < e_prev_n.y)
            ans.push_back('R');
        e = e_prev;
    }
    std::reverse(ans.begin(), ans.end());
    std::cout << ans << std::endl;
    return 0;
}