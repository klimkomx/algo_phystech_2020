#include <iostream>
#include <vector>

const long long MOD = 999999937;

inline void input_optimization() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(false);
}


long long cntstrings(long long& n) {
    --n;
    std::vector<std::vector<long long> > pow(5, std::vector<long long>(5, 0)),
                                          base(5, std::vector<long long>(5, 1)),
                                          copy;

    for (int i = 0; i < 5; ++i)
        pow[i][i] = 1;
    base[2][3] = base[2][4] = base[4][3] = base[4][4] = 0;

    while (n != 0) {
        if (n % 2 == 1) {
            copy = pow;
            for (int i = 0; i < 5; ++i)
                for (int j = 0; j < 5; ++j) {
                    pow[i][j] = 0;
                    for (int k = 0; k < 5; ++k)
                        pow[i][j] = (pow[i][j] + copy[i][k] * base[k][j]) % MOD;
                }
            --n;
        } else {
            copy = base;
            for (int i = 0; i < 5; ++i)
                for (int j = 0; j < 5; ++j) {
                    base[i][j] = 0;
                    for (int k = 0; k < 5; ++k)
                        base[i][j] = (base[i][j] + copy[i][k] * copy[k][j]) % MOD;
                }
            n /= 2;
        }
    }
    long long sum = 0;
    for (int i = 0; i < 5; ++i)
        for (int j = 0; j < 5; ++j)
            sum = (sum + pow[i][j]) % MOD;
    return sum;
}

int32_t main() {
    input_optimization();
    long long n;
    std::cin >> n;

    while (n != 0) {
        std::cout << cntstrings(n) << std::endl;
        std::cin >> n;
    }
    return 0;
}