 #include <iostream>
#include <set>
#include <algorithm>
#include <vector>
#include <map>
#include <string>

class mySet {
private:
    std::vector<std::set<long long>> sets;
    std::map<long long, std::set<int>> elements;

    template<typename T>
    static std::vector<T> printList(const std::set<T>& s);
public:
    void resize(int m);
    void add(long long e, int s);
    void del(long long e, int s);
    void clear(int s);
    std::vector<long long> listset(int s);
    std::vector<int> listsetof(long long e);
};

template<typename T>
std::vector<T> mySet::printList(const std::set<T>& s) {
    auto it = s.begin();
    std::vector<T> ans;
    while (it != s.end()) {
        ans.push_back(*it);
        ++it;
    }
    return ans;
}

void mySet::resize(int m) {
    sets.resize(m + 1);
}

void mySet::add(long long e, int s) {
    elements[e].insert(s);
    sets[s].insert(e);
}

void mySet::del(long long e, int s) {
    elements[e].erase(s);
    sets[s].erase(e);
}

void mySet::clear(int s) {
    std::vector<long long> a = printList(sets[s]);
    sets[s].clear();
    for (int i = 0; i < a.size(); ++i) {
        elements[a[i]].erase(s);
    }
}

std::vector<long long> mySet::listset(int s) {
    return printList(sets[s]);
}

std::vector<int> mySet::listsetof(long long e) {
    return printList(elements[e]);
}

int main() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
    long long n, e;
    int m, k, s;
    std::string command;
    std::vector<long long> resultl;
    std::vector<int> result;
    std::cin >> n >> m >> k;
    mySet st;
    st.resize(m);

    for (int i = 0; i < k; ++i) {
        std::cin >> command;
        if (command == "ADD") {
            std::cin >> e >> s;
            st.add(e, s);
        } else if (command == "DELETE") {
            std::cin >> e >> s;
            st.del(e, s);
        } else if (command == "CLEAR") {
            std::cin >> s;
            st.clear(s);
        } else if (command == "LISTSET") {
            std::cin >> s;
            resultl = st.listset(s);
            for (int i = 0; i < resultl.size(); ++i) {
                std::cout << resultl[i] << " ";
            }
            if (resultl.empty())
                std::cout << -1;
            std::cout << "\n";
        } else if (command == "LISTSETSOF") {
            std::cin >> e;
            result = st.listsetof(e);
            for (int i = 0; i < result.size(); ++i) {
                std::cout << result[i] << " ";
            }
            if (result.empty())
                std::cout << -1;
            std::cout << "\n";
        }
    }
    return 0;
}