#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void input_optimisation() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
}

std::vector<int> longest_not_increasing_subseq(const std::vector<int>& a, const int n) {
    std::vector<int> dp(n + 1, INT32_MAX),
                     index(n + 1, -1),
                     prev(n + 1, 0);
    dp[0] = INT32_MIN;
    int brd;
    for (int i = 0; i < n; ++i) {
        brd = static_cast<int>(std::upper_bound(dp.begin(), dp.end(), a[i]) - dp.begin());
        if (dp[brd - 1] <= a[i] && a[i] <= dp[brd]) {
            dp[brd] = a[i];
            index[brd] = i;
            prev[i] = index[brd - 1];
        }
    }
    int l = n;
    while (dp[l] == INT32_MAX) --l;
    std::vector<int> ans;
    int elem = index[l];
    while (elem != -1) {
        ans.push_back(elem + 1);
        elem = prev[elem];
    }
    return ans;
}
int32_t main() {
    input_optimisation();
    int n;

    std::cin >> n;
    std::vector<int> a(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
        a[i] = -a[i];
    }

    std::vector<int> ans = longest_not_increasing_subseq(a, n);


    std::cout << ans.size() << std::endl;
    for (int i = ans.size() - 1; i >= 0; --i) {
        std::cout << ans[i] << " ";
    }
    return 0;
}