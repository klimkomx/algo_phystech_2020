    #include <iostream>
    #include <vector>
     
    using std::vector;
    using std::pair;
    using std::cin;
    using std::cout;
     
    long prim(vector<vector<pair<int, int> > >& graph, int n) {
        vector<int> dt(n, INT32_MAX);
        vector<bool> used(n, false);
        dt[0] = 0;
        long weights_sum = 0l;
        for (int i = 0; i < n; ++i) {
            int bt = -1;
            for (int i = 0; i < n; ++i)
                if (!used[i] && (bt == -1 || dt[i] < dt[bt]))
                    bt = i;
            if (bt == -1)
                break;
            used[bt] = true;
            weights_sum += dt[bt];
            for (auto& e : graph[bt]) {
                if (e.second < dt[e.first])
                    dt[e.first] = e.second;
            }
        }
        return weights_sum;
    }
     
    int main() {
        std::ios::sync_with_stdio(0);
        cin.tie(0);
     
        int n, m, a, b, c;
     
        cin >> n >> m;
     
        vector<vector<pair<int, int> > > g(n);
     
        for (int i = 0; i < m; ++i) {
            cin >> a >> b >> c;
            --a, --b;
            g[a].push_back({b, c});
            g[b].push_back({a, c});
        }
     
        cout << prim(g, n);
        return 0;
    }