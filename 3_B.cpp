#include <utility>
#include <iostream>

struct splayTreeNode{
    splayTreeNode* left;
    splayTreeNode* right;
    splayTreeNode* parent;
    int val;
    long long sum;

    splayTreeNode();
    splayTreeNode(int x);

    ~splayTreeNode();
};

splayTreeNode::splayTreeNode():
        left(nullptr),
        right(nullptr),
        parent(nullptr),
        val(0),
        sum(0ll)
{}

splayTreeNode::splayTreeNode(int x):
        left(nullptr),
        right(nullptr),
        parent(nullptr),
        val(x),
        sum(x)
{}

splayTreeNode::~splayTreeNode()
{
    if (left)
        delete left;
    if (right)
        delete right;
}

class splayTree{
private:
    splayTreeNode * root;

    splayTreeNode * splay (splayTreeNode*);
    splayTreeNode * find(splayTreeNode*, int);
    std::pair<splayTreeNode*, splayTreeNode*> split(splayTreeNode*, int);
    splayTreeNode * insert(splayTreeNode*, int);
    splayTreeNode * merge(splayTreeNode*, splayTreeNode*);
    splayTreeNode * erase(splayTreeNode*, int);
    splayTreeNode * erase(int);
public:
    splayTree();

    static inline void setParent(splayTreeNode*, splayTreeNode*);
    static inline void restoreParent(splayTreeNode*);
    static inline long long getSum(splayTreeNode*);
    static inline void updateSum(splayTreeNode*);
    static void rotate(splayTreeNode*, splayTreeNode*);

    void insert(int);
    long long sum(int, int);
};

splayTree::splayTree() :
        root(nullptr)
{}

inline void splayTree::setParent(splayTreeNode * child, splayTreeNode * parent)
{
    if (child != nullptr)
        child -> parent = parent;
}

inline void splayTree::restoreParent(splayTreeNode * v)
{
    if (v == nullptr)
        return;
    setParent(v -> left, v);
    setParent(v -> right, v);
}

inline long long splayTree::getSum(splayTreeNode * v) {
    return (v == nullptr) ? 0ll : v -> sum;
}

inline void splayTree::updateSum(splayTreeNode * v) {
    if (v != nullptr)
        v -> sum = ((long long)v -> val) + getSum(v -> left) + getSum(v -> right);
}

void splayTree::rotate(splayTreeNode * parent, splayTreeNode * child)
{
    splayTreeNode * gp = parent -> parent;
    if (gp != nullptr)
        if (gp -> left == parent)
            gp -> left = child;
        else
            gp -> right = child;

    if (parent -> left == child){
        parent -> left = child -> right;
        child -> right = parent;
    } else{
        parent -> right = child -> left;
        child -> left = parent;
    }

    restoreParent(child);
    restoreParent(parent);

    updateSum(parent);
    updateSum(child);
    updateSum(gp);

    child -> parent = gp;
}

splayTreeNode* splayTree::splay (splayTreeNode * v){
    if (v -> parent == nullptr)
        return v;
    splayTreeNode * p = v -> parent;
    splayTreeNode * gp = p -> parent;
    if (gp == nullptr) {
        rotate(p, v);
        return v;
    }

    bool zigzig = (gp -> left == p) == (p -> left == v);

    if (zigzig){
        rotate(gp, p);
        rotate(p, v);
    } else{
        rotate(p, v);
        rotate(gp, v);
    }
    return splay(v);
}

splayTreeNode* splayTree::find(splayTreeNode* v, int val){
    if (v == nullptr)
        return nullptr;
    if (v -> val == val)
        return splay(v);
    if (val < v -> val && v -> left != nullptr)
        return find(v -> left, val);
    if (val > v -> val && v -> right != nullptr)
        return find(v -> right, val);
    return splay(v);
}

std::pair<splayTreeNode*, splayTreeNode*> splayTree::split(splayTreeNode * rt, int val){
    if (rt == nullptr)
        return {nullptr, nullptr};
    rt = find(rt, val);
    if (rt -> val >= val){
        std::pair<splayTreeNode *, splayTreeNode *> ans = {rt -> left, rt};
        rt -> left = nullptr;
        updateSum(rt);
        setParent(ans.first, nullptr);
        return ans;
    } else {
        splayTreeNode * r = rt -> right;
        setParent(r, nullptr);
        rt -> right = nullptr;
        updateSum(rt);
        return {rt, r};
    }
}

splayTreeNode* splayTree::insert(splayTreeNode * rt, int val){
    std::pair<splayTreeNode *, splayTreeNode *> res = split(rt, val);
    rt = new splayTreeNode(val);
    rt -> left = res.first;
    rt -> right = res.second;
    updateSum(rt);
    restoreParent(rt);
    return rt;
}

splayTreeNode* splayTree::merge(splayTreeNode * left, splayTreeNode * right){
    if (left == nullptr)
        return right;
    if (right == nullptr)
        return left;
    right = find(right, left->val);
    right -> left = left;
    left -> parent = right;
    updateSum(right);
    return right;
}

splayTreeNode* splayTree::erase(splayTreeNode * v, int val){
    splayTreeNode * rt = find(root, val);
    if (rt == nullptr || rt -> val != val)
        return rt;
    setParent(rt -> left, nullptr);
    setParent(rt -> right, nullptr);
    std::pair <splayTreeNode *, splayTreeNode *> ans = {rt -> left, rt -> right};
    rt -> left = rt -> right = nullptr;
    delete rt;
    return merge(ans.first, ans.second);
}

splayTreeNode * splayTree::erase(int val){
    root = erase(root, val);
}

void splayTree::insert(int val) {
    root = find(root, val);
    if (root == nullptr || root -> val != val)
        root = insert(root, val);
}


long long splayTree::sum(int l, int r) {
    std::pair<splayTreeNode*, splayTreeNode*> ftSplit, sdSplit;
    ftSplit = split(root, l);
    sdSplit = split(ftSplit.second, r + 1);
    long long ans = getSum(sdSplit.first);
    root = merge(ftSplit.first, merge(sdSplit.first, sdSplit.second));
    return ans;
}

int32_t main() {
    int n, l, r, idx;
    long long sm = 0ll;
    splayTree tree;
    char command;

    std::cin >> n;
    for (int i = 0; i < n; ++i) {
        std::cin >> command;
        if (command == '+') {
            std::cin >> idx;
            tree.insert((idx + (int)(sm%(long long)1e9)) % (int)1e9);
            sm = 0ll;
        } else {
            std::cin >> l >> r;
            sm = tree.sum(l, r);
            std::cout << sm << "\n";
        }
    }
    return 0;
}
