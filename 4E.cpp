    #include <iostream>
    #include <vector>
     
    class SegmentTree {
        SegmentTree *l, *r;
        long long sum;
    public:
        SegmentTree();
        ~SegmentTree();
        SegmentTree(SegmentTree& other) = delete;
     
        void change(int lb, int rb, long long c);
        long long query(int lb, int rb, int pf) const;
    };
     
    SegmentTree::SegmentTree() {
        sum = 0;
        l = r = nullptr;
    }
    SegmentTree::~SegmentTree() {
        if (l != nullptr)
            delete l;
        if (r != nullptr)
            delete r;
    }
    void SegmentTree::change(int lb, int rb, long long c) {
        if (lb + 1 == rb) {
            sum += c;
            return;
        }
        int m = lb + (rb - lb) / 2;
        if (c < m){
            if (l == nullptr) l = new SegmentTree();
            l -> change(lb, m, c);
        } else {
            if (r == nullptr) r = new SegmentTree();
            r -> change(m, rb, c);
        }
        sum = 0ll;
        if (l != nullptr)
            sum += l -> sum;
        if (r != nullptr)
            sum += r -> sum;
        return;
    }
    long long SegmentTree::query(int lb, int rb, int pf) const{
        if (rb <= pf)
            return sum;
        long long sumw = 0ll;
        int m = lb + (rb - lb) / 2;
        if (pf < m) {
            if (l != nullptr) sumw += l -> query(lb, m, pf);
        } else {
            if (l != nullptr) sumw += l -> sum;
            if (r != nullptr) sumw += r -> query(m, rb, pf);
        }
        return sumw;
    }
     
    int main() {
        std::cin.tie(0);
        std::ios::sync_with_stdio(0);
     
        const int MAXX = 1e9;
        int q, x;
        char com;
     
        SegmentTree tr;
     
        std::cin >> q;
     
        for (int i = 0; i < q; ++i) {
            std::cin >> com >> x;
            if (com == '+')
                tr.change(0, MAXX, x);
            else
                std::cout << tr.query(0, MAXX, x + 1) << "\n";
        }
        return 0;
    }