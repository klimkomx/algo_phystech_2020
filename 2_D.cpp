#include <iostream>

template <typename T>
class vector
{
public:
    vector();
    vector(int defSize);
    ~vector();
    vector(const vector<T>& otherVector);


    vector& operator= (const vector<T>& otherVector);
    T& operator[] (int i);

    int size();
    bool empty();
    void push_back(const T& newElement);
    void pop_back();
    void clear();
private:
    T* body;
    const int defaultBufferSize = 5000;
    int bufferSize;
    int vectorSize;
    void shrinkBuffer();
    void growBuffer();
};

template <typename T>
vector<T>::vector():
        vectorSize(0)
{
    bufferSize = defaultBufferSize;
    body = new T[bufferSize];
}

template <typename T>
vector<T>::vector(int defSize):
        vectorSize(0)
{
    bufferSize = defSize;
    body = new T[bufferSize];
}

template <typename T>
vector<T>::~vector()
{
    delete body;
}

template <typename T>
vector<T>::vector(const vector<T>& otherVector)
{
    bufferSize = otherVector.bufferSize;
    vectorSize = otherVector.vectorSize;
    body = new T[otherVector.vectorSize];

    for (int i = 0; i < vectorSize; ++i)
    {
        body[i] = otherVector.body[i];
    }
}

template <typename T>
vector<T>& vector<T>::operator=(const vector<T> &otherVector)
{
    if (&otherVector != this) {
        delete body;
        bufferSize = otherVector.bufferSize;
        vectorSize = otherVector.vectorSize;
        body = new T[otherVector.vectorSize];

        for (int i = 0; i < vectorSize; ++i) {
            body[i] = otherVector.body[i];
        }
    }
    return *this;
}

template <typename T>
T& vector<T>::operator[](int i)
{
    return body[i];
}

template <typename T>
int vector<T>::size()
{
    return vectorSize;
}

template <typename T>
bool vector<T>::empty()
{
    return vectorSize == 0;
}

template <typename T>
void vector<T>::push_back(const T& newElement)
{
    if (vectorSize == bufferSize)
    {
        growBuffer();
    }
    body[vectorSize++] = newElement;
}

template<typename T>
void vector<T>::pop_back()
{
    --vectorSize;
    if (4 * vectorSize <= bufferSize && bufferSize >= 2 * defaultBufferSize)
    {
        shrinkBuffer();
    }
}

template <typename T>
void vector<T>::clear()
{
    delete body;
    bufferSize = defaultBufferSize;
    vectorSize = 0;
    body = new T[bufferSize];
}

template <typename T>
void vector<T>::growBuffer()
{
    bufferSize *= 2;
    T* newBody = new T[bufferSize];
    for (int i = 0; i < vectorSize; ++i)
    {
        newBody[i] = body[i];
    }
    delete body;
    body = newBody;
}

template <typename T>
void vector<T>::shrinkBuffer()
{
    bufferSize /= 2;
    T* newBody = new T[bufferSize];

    for (int i = 0; i < bufferSize; ++i)
    {
        newBody[i] = body[i];
    }
    delete body;
    body = newBody;
}

template <typename T>
void swap(T& _t1, T& _t2)
{
    T tmp(_t1);
    _t1 = _t2;
    _t2 = tmp;
}

//HEAP
template <typename T>
class BinaryHeap
{
public:
    BinaryHeap();
    BinaryHeap(const vector<T>& elements);
    BinaryHeap(const BinaryHeap& otherHeap);
    ~BinaryHeap();

    T getMin();
    void eraseMin();
    void insert(const T& element);
    void clear();
    int heapSize() const;
private:
    vector<T> heapBody;
    int size;

    void siftUp(int i);
    void siftDown(int i);

};

template <typename T>
BinaryHeap<T>::BinaryHeap():
        heapBody(vector<T>()),
        size(0)
{
}

template<typename T>
BinaryHeap<T>::~BinaryHeap()
{
    heapBody.clear();
}

template<typename T>
BinaryHeap<T>::BinaryHeap(const vector<T> &elements)
{
    heapBody = elements;
    size = heapBody.size();
    for (int i = size - 1; i >= 0; --i)
    {
        siftDown(i);
    }
}

template<typename T>
BinaryHeap<T>::BinaryHeap(const BinaryHeap<T>& otherHeap) {
    heapBody = otherHeap.heapBody;
    size = otherHeap.size;
}

template<typename T>
void BinaryHeap<T>::siftDown(int i)
{
    while (2 * i + 1 < size)
    {
        int lc = 2 * i + 1;
        int rc = lc + 1;
        int toswp = lc;

        if (rc < size && heapBody[rc] < heapBody[toswp])
        {
            toswp = rc;
        }
        if (heapBody[toswp] < heapBody[i])
        {
            swap(heapBody[i], heapBody[toswp]);
            i = toswp;
        } else
        {
            break;
        }

    }
}

template<typename T>
void BinaryHeap<T>::siftUp(int i)
{
    while (i != 0 && heapBody[i] < heapBody[(i - 1) / 2])
    {
        swap(heapBody[i], heapBody[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

template<typename T>
T BinaryHeap<T>::getMin()
{
    return heapBody[0];
}

template<typename T>
void BinaryHeap<T>::eraseMin()
{
    if (size != 0)
    {
        heapBody[0] = heapBody[size - 1];
    }
    heapBody.pop_back();

    --size;
    siftDown(0);
}

template<typename T>
void BinaryHeap<T>::insert(const T& element)
{
    heapBody.push_back(element);
    ++size;
    siftUp(size - 1);
}

template <typename T>
void BinaryHeap<T>::clear()
{
    heapBody.clear();
    size = 0;
}

template<typename T>
int BinaryHeap<T>::heapSize() const
{
    return size;
}

template<typename T>
class Heap
{
public:
    Heap();
    ~Heap();
    Heap(const Heap<T>& otherHeap);

    void insert(T& element);
    T getMin();
    void eraseMin();
    void decreaseKey(int index, T& delta);
    void clean();
private:

    BinaryHeap<T> originalHeap, decreasedElementsHeap;
    vector<T> queries;
};

template<typename T>
Heap<T>::Heap():
        originalHeap(BinaryHeap<T>()),
        decreasedElementsHeap(BinaryHeap<T>()),
        queries(vector<T>())
{
}

template<typename T>
Heap<T>::~Heap()
{
    originalHeap.clear();
    decreasedElementsHeap.clear();
    queries.clear();
}

template<typename T>
Heap<T>::Heap(const Heap<T>& otherHeap)
{
    originalHeap = otherHeap.originalHeap;
    decreasedElementsHeap = otherHeap.decreasedElementsHeap;
    queries = otherHeap.queries;
}

template <typename T>
void Heap<T>::clean()
{
    while (decreasedElementsHeap.heapSize() &&
           decreasedElementsHeap.getMin() == originalHeap.getMin()) {
        originalHeap.eraseMin();
        decreasedElementsHeap.eraseMin();
    }
}

template<typename T>
void Heap<T>::insert(T &element)
{
    queries.push_back(element);
    originalHeap.insert(element);
}

template<typename T>
T Heap<T>::getMin()
{
    queries.push_back(T(0));
    return originalHeap.getMin();
}

template<typename T>
void Heap<T>::eraseMin()
{
    queries.push_back((T)0);
    originalHeap.eraseMin();
}

template<typename T>
void Heap<T>::decreaseKey(int index, T &delta)
{
    originalHeap.insert(queries[index] - delta);
    decreasedElementsHeap.insert(queries[index]);
    queries[index] -= delta;
    queries.push_back((T)0);
}

int main()
{
    std::cin.tie(nullptr);
    std::ios::sync_with_stdio(0);

    long long q, ftArg, sdArg;
    std::cin >> q;
    std::string command;
    Heap<long long> hp;
    for (int i = 0; i < q; ++i)
    {
        std::cin >> command;
        if (command == "insert")
        {
            std::cin >> ftArg;
            hp.insert(ftArg);
        } else if (command == "getMin")
        {
            hp.clean();
            std::cout << hp.getMin() << "\n";
        } else if (command == "extractMin")
        {
            hp.clean();
            hp.eraseMin();
        } else
        {
            std::cin >> ftArg >> sdArg;
            hp.decreaseKey(ftArg - 1, sdArg);
        }
    }
    return 0;
}