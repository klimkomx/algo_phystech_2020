#include <vector>
#include <type_traits>
#include <memory>
#include <iostream>
#include <queue>

using std::shared_ptr;

//FIXED
template<size_t chunkSize>
class FixedAllocator {
private:
    size_t BIG_CHUNK_SIZE = 16;
    static FixedAllocator<chunkSize>* instance;
    size_t it = BIG_CHUNK_SIZE - 1;
    std::vector<char*> chunks;
    std::queue<char*> emptychunks;

    FixedAllocator() = default;
    void makeLongChunk();

public:
    FixedAllocator(FixedAllocator const &) = delete;
    FixedAllocator& operator=(FixedAllocator const&) = delete;

    static FixedAllocator<chunkSize>& getInstance();
    char* makeChunk();
    void deleteChunk(char* ptr, size_t n);
    ~FixedAllocator();
};

template <size_t chunkSize>
FixedAllocator<chunkSize>* FixedAllocator<chunkSize>::instance = nullptr;

template<size_t chunkSize>
FixedAllocator<chunkSize>::~FixedAllocator() {
    for(size_t i = 0; i < chunks.size(); ++i) {
        delete[] chunks[i];
    }
}

template<size_t chunkSize>
FixedAllocator<chunkSize>& FixedAllocator<chunkSize>::getInstance() {
    if (instance == nullptr)
        instance = new FixedAllocator<chunkSize>();
    return *instance;
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::makeLongChunk() {
    BIG_CHUNK_SIZE *= 2;
    chunks.push_back(new char[BIG_CHUNK_SIZE * chunkSize]);
    it = 0;
}

template<size_t chunkSize>
char* FixedAllocator<chunkSize>::makeChunk() {
    if (!emptychunks.empty()) {
        auto q = emptychunks.front();
        emptychunks.pop();
        return q;
    }
    if (it == BIG_CHUNK_SIZE - 1) {
        makeLongChunk();
    }
    return chunks.back() + (it++) * chunkSize;
}

template<size_t chunkSize>
void FixedAllocator<chunkSize>::deleteChunk(char* mem, size_t) {
    emptychunks.push(mem);
}

//FAST
template<typename T>
class FastAllocator {
private:
    static const size_t maxSize = 24;
    static const size_t fixSize = sizeof(T);
public:
    FastAllocator() = default;

    template<typename V>
    FastAllocator(const FastAllocator<V>&){};
    ~FastAllocator() = default;
    static T* allocate(size_t n);
    static void deallocate(T* ptr, size_t n);

    using value_type = T;
    template<typename U>
    struct rebind {
        typedef FastAllocator<U> other;
    };
};


template<typename T>
T* FastAllocator<T>::allocate(size_t n) {
    if (sizeof(T) <= maxSize && n <= 1) {
        return reinterpret_cast<T*>(FixedAllocator<fixSize>::getInstance().makeChunk());
    } else {
        return reinterpret_cast<T*>(new char[n * sizeof(T)]);
    }
}

template<typename T>
void FastAllocator<T>::deallocate(T *ptr, size_t n) {
    if (sizeof(T) <= maxSize && n <= 1){
        FixedAllocator<maxSize>::getInstance().deleteChunk(reinterpret_cast<char*>(ptr), n);
    } else {
        delete[] reinterpret_cast<char*>(ptr);
    }
}

//LIST

template<typename T, typename Allocator = std::allocator<T>>
class List {
private:
    struct NodeBase {
        NodeBase* prev = nullptr;
        NodeBase* next = nullptr;
        NodeBase() = default;
        NodeBase(const NodeBase&) = default;
        ~NodeBase() = default;
    };
    struct Node : NodeBase {
        T value;
        explicit Node(const T& val): NodeBase(), value(val){};
        explicit Node(): NodeBase(), value(){};
        ~Node() = default;
    };
    using AllocatorTraits = typename std::allocator_traits<Allocator>::template rebind_traits<Node>;
    using AllocatorType = typename std::allocator_traits<Allocator>::template rebind_alloc<Node>;
    AllocatorType allocator_;
    Allocator allocator_m;
    size_t size_;
    NodeBase* list;


    Node* makeNewNode(const T& elem);
    void eraseNode(Node*);
    void connect(NodeBase*, NodeBase*);
public:
    explicit List(const Allocator& alloc = Allocator());
    List(size_t count, const Allocator& alloc = Allocator());
    List(size_t count, const T& value, const Allocator& alloc = Allocator());
    List(const List& other);
    List<T, Allocator>& operator=(const List& other);
    ~List();

    Allocator get_allocator();
    size_t size() const;
    void push_back(const T& elem);
    void push_front(const T& elem);
    void pop_back();
    void pop_front();

    friend class iteratorTemp;

    template<typename vtype>
    class iteratorTemp {
    protected:
        NodeBase* obj;
    public:
        friend class List<T, Allocator>;

        iteratorTemp() = delete;
        explicit iteratorTemp(List<T, Allocator>::NodeBase* const node): obj(node){};
        iteratorTemp(const List<T, Allocator>::iteratorTemp<vtype>& other) = default;
        ~iteratorTemp() = default;
        //increment/decrement
        iteratorTemp& operator++();
        iteratorTemp& operator--();
        iteratorTemp operator++(int);
        iteratorTemp operator--(int);
        bool operator==(const iteratorTemp& value) const;
        bool operator!=(const iteratorTemp& value) const;

        vtype& operator*() const;
        vtype* operator->() const;

        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = T;
        using difference_type = long long;
        using reference = vtype&;
        using pointer = vtype*;

        operator List<T, Allocator>::iteratorTemp<const value_type>() const;
    };

    typedef iteratorTemp<T> iterator;
    typedef iteratorTemp<const T> const_iterator;
    typedef std::reverse_iterator<iterator> reverse_iterator;
    typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    reverse_iterator rend();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator rend() const;
    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    template<typename vtype>
    iteratorTemp<vtype> insert(const iteratorTemp<vtype>&, const T&);

    template<typename vtype>
    void erase(const iteratorTemp<vtype>&);
};

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> &List<T, Allocator>::iteratorTemp<vtype>::operator++() {
    obj = obj ->next;
    return *this;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> &List<T, Allocator>::iteratorTemp<vtype>::operator--() {
    obj = obj ->prev;
    return *this;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::iteratorTemp<vtype>::operator++(int) {
    auto tmp = this;
    obj = obj ->next;
    return *tmp;
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::iteratorTemp<vtype>::operator--(int) {
    auto tmp = this;
    obj = obj ->prev;
    return *tmp;
}

template<typename T, typename Allocator>
template<typename vtype>
bool List<T, Allocator>::template iteratorTemp<vtype>::operator==(const List::iteratorTemp<vtype> &value) const {
    return obj == value.obj;
}

template<typename T, typename Allocator>
template<typename vtype>
bool List<T, Allocator>::template iteratorTemp<vtype>::operator!=(const List::iteratorTemp<vtype> &value) const {
    return obj != value.obj;
}

template<typename T, typename Allocator>
template<typename vtype>
vtype& List<T, Allocator>::iteratorTemp<vtype>::operator*() const {
    return reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename vtype>
vtype* List<T, Allocator>::iteratorTemp<vtype>::operator->() const {
    return &reinterpret_cast<Node*>(obj) ->value;
}

template<typename T, typename Allocator>
template<typename vtype>
List<T, Allocator>::iteratorTemp<vtype>::operator List<T, Allocator>::iteratorTemp<const value_type>() const {
    return List::iteratorTemp<const value_type>(obj);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const Allocator &alloc):allocator_(typename std::allocator_traits<Allocator>::template rebind_alloc<Node>(alloc)),
                                                 allocator_m(alloc), size_(0), list(new NodeBase()) {
    connect(list, list);
}

template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const Allocator &alloc):List(alloc) {
    size_ = count;
    while (count--) {
        Node* newNode = AllocatorTraits::allocate(allocator_, 1);
        AllocatorTraits::construct(allocator_, newNode);
        connect(list -> prev, newNode);
        connect(newNode, list);
    }
}
template<typename T, typename Allocator>
List<T, Allocator>::List(size_t count, const T &value, const Allocator &alloc):List(alloc) {
    while (count--) {
        push_back(value);
    }
}

template<typename T, typename Allocator>
List<T, Allocator>::List(const List &other):size_(0), list(new NodeBase()){
    connect(list, list);
    allocator_ = AllocatorTraits::select_on_container_copy_construction(other.allocator_);
    allocator_m = std::allocator_traits<Allocator>::select_on_container_copy_construction(other.allocator_m);
    auto it = other.begin();
    while (it != other.end()) {
        push_back(*it);
        ++it;
    }
}

template<typename T, typename Allocator>
List<T, Allocator> &List<T, Allocator>::operator=(const List &other) {
    while (size_)
        pop_back();
    allocator_ = AllocatorTraits::propagate_on_container_copy_assignment::value ? other.allocator_ : allocator_;
    allocator_m = std::allocator_traits<Allocator>::propagate_on_container_copy_assignment::value ?
                  other.allocator_m : allocator_m;
    auto it = other.begin();
    while (it != other.end()) {
        push_back(*it);
        ++it;
    }
    return *this;
}

template<typename T, typename Allocator>
Allocator List<T, Allocator>::get_allocator() {
    return allocator_m;
}

template<typename T, typename Allocator>
size_t List<T, Allocator>::size() const{
    return size_;
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_back(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(list->prev, newNode);
    connect(newNode, list);
}

template<typename T, typename Allocator>
void List<T, Allocator>::push_front(const T &elem) {
    Node* newNode = makeNewNode(elem);
    connect(newNode, list->next);
    connect(list, newNode);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_back() {
    Node* copy = reinterpret_cast<Node*>(list -> prev);
    connect(copy -> prev, list);
    eraseNode(copy);
}

template<typename T, typename Allocator>
void List<T, Allocator>::pop_front() {
    Node* copy = reinterpret_cast<Node*>(list -> next);
    connect(list, copy -> next);
    eraseNode(copy);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::Node *List<T, Allocator>::makeNewNode(const T& elem) {
    ++size_;
    Node* newNode = AllocatorTraits::allocate(allocator_, 1);
    AllocatorTraits::construct(allocator_, newNode, elem);
    return newNode;
}

template<typename T, typename Allocator>
void List<T, Allocator>::eraseNode(typename List<T, Allocator>::Node * node) {
    --size_;
    AllocatorTraits::destroy(allocator_, node);
    AllocatorTraits::deallocate(allocator_, node, 1);
}

template<typename T, typename Allocator>
void List<T, Allocator>::connect(NodeBase * ft, NodeBase * sd) {
    ft -> next = sd;
    sd ->prev = ft;
}


template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::begin() {
    return List::iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::iterator List<T, Allocator>::end() {
    return List::iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::begin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::end() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cbegin() const {
    return List::const_iterator(list -> next);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_iterator List<T, Allocator>::cend() const {
    return List::const_iterator(list);
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rbegin() {
    return List::reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::reverse_iterator List<T, Allocator>::rend() {
    return List::reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rbegin() const {
    return List::const_reverse_iterator(end());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::rend() const {
    return List::const_reverse_iterator(begin());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crbegin() const {
    return List::const_reverse_iterator(cend());
}

template<typename T, typename Allocator>
typename List<T, Allocator>::const_reverse_iterator List<T, Allocator>::crend() const {
    return List::const_reverse_iterator(cbegin());
}

template<typename T, typename Allocator>
template<typename vtype>
typename List<T, Allocator>::template iteratorTemp<vtype> List<T, Allocator>::insert(const List::iteratorTemp<vtype>& it, const T &val) {
    Node* node = makeNewNode(val);
    connect((it.obj) -> prev, node);
    connect(node, it.obj);
    return iteratorTemp<vtype>(node);
}

template<typename T, typename Allocator>
template<typename vtype>
void List<T, Allocator>::erase(const List::iteratorTemp<vtype>& it) {
    connect((it.obj) -> prev, (it.obj) -> next);
    eraseNode(reinterpret_cast<Node*>(it.obj));
}

template<typename T, typename Allocator>
List<T, Allocator>::~List() {
    auto it = --end();
    while (size() != 0) {
        auto copy = it;
        --it;
        erase(copy);
    }
    delete list;
}