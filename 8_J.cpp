#include<vector>
#include<stack>
#include<iostream>
#include <queue>

using std::vector;

struct IGraph {
    virtual ~IGraph() = default;

    virtual void addEdge( int from, int to ) = 0;
    virtual void deleteEdge(int from, int to) = 0;
    virtual vector<int> getNextVerticies( int from ) const = 0;
    virtual bool hasEdge( int from, int to ) const = 0;
    virtual size_t size() const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph( size_t vertexCount );
    ListGraph( IGraph* graph );
    virtual ~ListGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<int>> lists;
};

ListGraph::ListGraph(size_t vertexCount): lists(vector<vector<int> >(vertexCount)) {

}
ListGraph::ListGraph( IGraph* graph ) :
        ListGraph( graph->size() ) {
    for( int i = 0; i < lists.size(); ++i )
        lists[i] = graph->getNextVerticies( i );
}
void ListGraph::addEdge(int from, int to) {
    lists[from].push_back( to );
}
void ListGraph::deleteEdge(int from, int to) {
    int pos = -1;
    for (int i = 0; i < lists[from].size(); ++i) {
        if (lists[from][i] == to) {
            pos = i;
            break;
        }
    }
    if (pos != -1 && pos != lists[from].size() - 1)
        std::swap(lists[from][pos], lists[from][lists[from].size() - 1]);
    if (pos != -1)
        lists[from].pop_back();
}

vector<int> ListGraph::getNextVerticies( int from ) const {
    return lists[from];
}
bool ListGraph::hasEdge(int from, int to) const {
    for ( int i = 0; i < lists[from].size(); ++i ) {
        if (lists[from][i] == to)
            return true;
    }
    return false;
}
size_t ListGraph::size() const {
    return lists.size();
}

class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(size_t vertexCount);
    MatrixGraph( IGraph* graph );
    virtual ~MatrixGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from ) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<bool>> matrix;
};
MatrixGraph::MatrixGraph(size_t vertexCount): matrix(vector<vector<bool> >(vertexCount,
                                                                        vector<bool>(vertexCount, false))) {}

MatrixGraph::MatrixGraph(IGraph *graph): MatrixGraph(graph->size()) {
    for (int i = 0; i < graph -> size(); ++i) {
        vector<int> list = graph -> getNextVerticies(i);
        for (auto j : list) {
            matrix[i][j] = true;
        }
    }
}
void MatrixGraph::addEdge( int from, int to ) {
    matrix[from][to] = true;
}
void MatrixGraph::deleteEdge(int from, int to) {
    matrix[from][to] = false;
}
vector<int> MatrixGraph::getNextVerticies(int from) const {
    vector<int> res;
    for (int i = 0; i < size(); ++i) {
        if (matrix[from][i])
            res.push_back(i);
    }
    return res;
}
bool MatrixGraph::hasEdge(int from, int to) const {
    return matrix[from][to];
}

size_t MatrixGraph::size() const {
    return matrix.size();
}

vector<int> EulerCycle(IGraph& graph, int from) {
    std::stack<int> vt;
    vector<int> list, ans;
    int vt_now;
    bool found;
    vt.push(from);
    while (!vt.empty()) {
        vt_now = vt.top();
        found = false;
        for (int i = 0; i < graph.size(); ++i) {
            if (graph.hasEdge(vt_now, i)) {
                vt.push(i);
                graph.deleteEdge(vt_now, i);
                found = true;
                break;
            }
        }
        if (!found) {
            vt.pop();
            ans.push_back(vt_now);
        }
    }
    return ans;
}
int main() {
    int n, a;
    std::cin >> n >> a;
    --a;
    MatrixGraph graph(n);
    int ;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            std::cin >> k;
            if (i != j && !k)
                graph.addEdge(i, j);
        }
    }

    vector<int> cycle = EulerCycle(graph, a);

    for (int i = cycle.size() - 1; i > 0; --i) {
        std::cout << cycle[i] + 1 << " " << cycle[i - 1] + 1<< std::endl;
    }
    return 0;
}
