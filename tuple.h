
#include <utility>
#include <type_traits>
#include <cstdlib>

template<typename... T>
class Tuple {};

namespace helper {
    template<size_t, typename...>
    struct type_getter;
    template<size_t i, typename head, typename... tail>
    struct type_getter<i, head, tail...> {
        using type = typename type_getter<i - 1, tail...>::type;
    };
    template<typename head, typename... tail>
    struct type_getter<0, head, tail...> {
        using type = head;
    };

    template<size_t i, typename head_type, typename... tail_type>
    struct element_getter_by_index {
        static typename type_getter<i, head_type, tail_type...>::type& get(Tuple<head_type, tail_type...> &tuple) {
            return element_getter_by_index<i - 1, tail_type...>::get(tuple.tail);
        }

        static const typename type_getter<i, head_type, tail_type...>::type& get(const Tuple<head_type, tail_type...> &tuple) {
            return element_getter_by_index<i - 1, tail_type...>::get(
                    const_cast<const Tuple<tail_type...> &>(tuple.tail));
        }

        static typename type_getter<i, head_type, tail_type...>::type&& get(Tuple<head_type, tail_type...> &&tuple) {
            return element_getter_by_index<i - 1, tail_type...>::get(std::forward<Tuple<tail_type...>>(tuple.tail));
        }
    };

    template<typename head_type, typename... tail_type>
    struct element_getter_by_index<0, head_type, tail_type...> {
        static typename type_getter<0, head_type, tail_type...>::type& get(Tuple<head_type, tail_type...> &tuple) {
            return tuple.head;
        }

        static const typename type_getter<0, head_type, tail_type...>::type& get(const Tuple<head_type, tail_type...> &tuple) {
            return tuple.head;
        }

        static typename type_getter<0, head_type, tail_type...>::type&& get(Tuple<head_type, tail_type...> &&tuple) {
            return std::forward<head_type>(tuple.head);
        }
    };

    template<size_t number, typename model, typename head, typename... tail>
    struct type_counter {
        static void check_if_only_once() {
            type_counter<number, model, tail...>::check_if_only_once();
        }
    };

    template<size_t number, typename model, typename... tail>
    struct type_counter<number, model, model, tail...> {
        static void check_if_only_once() {
            type_counter<number + 1, model, tail...>::check_if_only_once();
        }
    };

    template<typename model, typename head, typename... other>
    struct type_counter<2, model, head, other...> {
    };

    template<typename T, typename head_type, typename... tail_type>
    struct element_getter_by_type {
        static T& get(Tuple<head_type, tail_type...> &tuple) {
            return element_getter_by_type<T, tail_type...>::get(tuple.tail);
        }

        static const T& get(const Tuple<head_type, tail_type...> &tuple) {
            return element_getter_by_type(const_cast<const Tuple<tail_type...> &>(tuple.tail));
        }

        static T&& get(Tuple<head_type, tail_type...> &&tuple) {
            return element_getter_by_type<T, tail_type...>(std::forward<Tuple<tail_type...>>(tuple.tail));
        }
    };

    template<typename T, typename... tail_type>
    struct element_getter_by_type<T, T, tail_type...> {
        static T& get(Tuple<T, tail_type...> &tuple) {
            return tuple.head;
        }

        static const T& get(const Tuple<T, tail_type...> &tuple) {
            return tuple.head;
        }

        static T&& get(Tuple<T, tail_type...> &&tuple) {
            return std::forward<T>(tuple.head);
        }
    };

    template<typename...>
    struct concater;
}

template<typename head_type, typename... tail_types>
class Tuple<head_type, tail_types...> {
private:
    head_type head;
    Tuple<tail_types...> tail;
public:
    /////////constructors & destructors
    Tuple():head(), tail() {}
    Tuple(const head_type& ft, const tail_types&... other): head(ft), tail(other...) {}

    template<typename _head_type, typename... _tail_types,
            typename = typename std::enable_if_t<sizeof...(_tail_types) == sizeof...(tail_types), bool>,
            typename = typename std::enable_if_t<!std::is_same_v<typename std::remove_reference<_head_type>::type , Tuple<head_type, tail_types...> >, bool>>
    Tuple(_head_type&& ft_elem, _tail_types&&... sd_elem):
        head(std::forward<_head_type>(ft_elem)),tail(std::forward<_tail_types>(sd_elem)...){}

    Tuple(const Tuple& other):
        head(other.head), tail(other.tail){}

    template<typename _head_type, typename... _tail_types>
    Tuple(const Tuple<_head_type, _tail_types...>& other):
        head(other.head), tail(other.tail){}

    Tuple(Tuple&& other):
        head(std::forward<head_type>(other.head)), tail(std::move(other.tail)){}

    template<typename _head_type, typename... _tail_types>
    Tuple(Tuple<_head_type, _tail_types...>&& other):
        head(std::forward<_head_type>(other.head)), tail(std::move(other.tail)){}

    ~Tuple() {}

    ///////////operator=  & swap & <=>
    template<typename _head_type, typename... _tail_types>
    Tuple& operator=(const Tuple<_head_type, _tail_types...>& other);

    template<typename _head_type, typename... _tail_types>
    Tuple& operator=(Tuple<_head_type, _tail_types...>&& other);

    template<typename _head_type, typename... _tail_types>
    void swap(Tuple<_head_type, _tail_types...>& other);

    //friend
    template<size_t, typename...>
    friend struct helper::type_getter;

    template<size_t i, typename _head_type, typename... _tail_type>
    friend struct helper::element_getter_by_index;

    template<size_t number, typename model, typename head, typename... tail>
    friend struct helper::type_counter;

    template<typename T, typename _head_type, typename... tail_type>
    friend struct helper::element_getter_by_type;

    template<typename...>
    friend struct helper::concater;

    template<typename... elems>
    friend class Tuple;

    /////////////////////___________
    bool operator==(const Tuple& other) const;
    bool operator!=(const Tuple& other) const;
    bool operator<(const Tuple& other) const;
    bool operator>(const Tuple& other) const;
    bool operator<=(const Tuple& other) const;
    bool operator>=(const Tuple& other) const;
};

template<typename head_type, typename... tail_types>
template<typename _head_type, typename... _tail_types>
Tuple<head_type, tail_types...>& Tuple<head_type, tail_types...>::operator=(const Tuple<_head_type, _tail_types...>& other) {
    head = other.head;
    tail = other.tail;
    return *this;
}

template<typename head_type, typename... tail_types>
template<typename _head_type, typename... _tail_types>
Tuple<head_type, tail_types...>& Tuple<head_type, tail_types...>::operator=(Tuple<_head_type, _tail_types...>&& other) {
    std::swap(head, other.head);
    tail = std::move(other.tail);
    return *this;
}

template<typename head_type, typename... tail_types>
template<typename _head_type, typename... _tail_types>
void Tuple<head_type, tail_types...>::swap(Tuple<_head_type, _tail_types...>& other) {
    std::swap(head, other.head);
    tail.swap(other.tail);
}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator==(const Tuple<head_type, tail_types...>& other) const {
    return (head == other.head && tail == other.tail);
}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator!=(const Tuple<head_type, tail_types...>& other) const {
    return (head != other.head || tail != other.tail);
}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator<(const Tuple<head_type, tail_types...>& other) const {
    if (head != other.head)
        return head < other.head;
    return tail < other.tail;
}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator>(const Tuple<head_type, tail_types...>& other) const {
    if (head != other.head)
        return head > other.head;
    return tail > other.tail;}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator>=(const Tuple<head_type, tail_types...>& other) const {
    return !(*this < other);
}

template<typename head_type, typename... tail_types>
bool Tuple<head_type, tail_types...>::operator<=(const Tuple<head_type, tail_types...>& other) const {
    return !(*this > other);
}
template<>
class Tuple<> {
public:
    Tuple(){}
    Tuple(const Tuple&){}
    Tuple(Tuple&&){}
    ~Tuple(){}
    Tuple& operator=(const Tuple&) {return *this;}
    Tuple& operator=(Tuple&&) {return *this;}
    void swap(const Tuple&) {}
    bool operator==(const Tuple&) const {return true;}
    bool operator!=(const Tuple&) const {return false;}
    bool operator<(const Tuple&) const {return false;}
    bool operator>(const Tuple&) const {return false;}
    bool operator<=(const Tuple&) const {return true;}
    bool operator>=(const Tuple&) const {return true;}
};

///////////////////////////////////////////////////get by index
template<size_t i, typename... others>
decltype(auto) get(Tuple<others...>& tuple) {
    return helper::element_getter_by_index<i, others...>::get(tuple);
}

template<size_t i, typename... others>
decltype(auto) get(const Tuple<others...>& tuple) {
    return helper::element_getter_by_index<i, others...>::get(tuple);
}

template<size_t i, typename... others>
decltype(auto) get(Tuple<others...>&& tuple) {
    return helper::element_getter_by_index<i, others...>::get(std::forward<Tuple<others...>>(tuple));
}

/////////////////////////////get by type
template<typename T, typename... others>
T& get(Tuple<others...>& tuple) {
    return helper::element_getter_by_type<T, others...>::get(tuple);
}

template<typename T, typename... others>
const T& get(const Tuple<others...>& tuple) {
    return helper::element_getter_by_type<T, others...>::get(tuple);
}

template<typename T, typename... others>
T&& get(Tuple<others...>&& tuple) {
    return helper::element_getter_by_type<T, others...>::get(std::forward<Tuple<others...>>(tuple));
}

//////////////////////////////////////////////////////////////////tuplecat
namespace helper {
    template< class T >
    struct remove_cvref {
        typedef std::remove_cv_t<std::remove_reference_t<T>> type;
    };

    template<typename... T>
    struct length;

    template<typename Ft, typename... Other>
    struct length<Tuple<Ft, Other...>> {
        static constexpr size_t get() {
            return length<Tuple<Other...>>::get() + 1;
        }
    };
    template<>
    struct length<Tuple<>> {
        static constexpr size_t get() {
            return 0;
        }
    };

    template<typename...>
    struct merged_tuples;

    template<typename... T1, typename... T2, typename... Other>
    struct merged_tuples<Tuple<T1...>, Tuple<T2...>, Other...> {
        using type = typename merged_tuples<Tuple<T1..., T2...>, Other...>::type;
    };

    template<typename... T>
    struct merged_tuples<Tuple<T...>> {
        using type = Tuple<T...>;
    };

    template<typename>
    struct _next;

    template<typename Tp, typename... Tps>
    struct _next<Tuple<Tp, Tps...>> {
        using type = Tuple<Tps...>;
    };

    //    template<size_t s>
    //    struct Index_Seq {
    //        using type = Index_Seq<s - 1>, s;
    //    };

    template<typename Ret, size_t... Idx, typename Tpl>
    struct concater<Ret, std::integer_sequence<size_t, Idx...>, Tpl> {
        template<typename... Raw>
        static auto _do(Tpl&& tp, Raw&&... elems) {
            if constexpr(std::is_same_v<Tpl, Tuple<>>)
            return Ret(std::forward<Raw>(elems)...);
            else
                return Ret(std::forward<Raw>(elems)..., get<Idx>(std::forward<Tpl>(tp))...);
        }
    };

    template<typename Ret, size_t... Idx, typename Tpl1, typename Tpl2, typename... Tpls>
    struct concater<Ret, std::integer_sequence<size_t, Idx...>, Tpl1, Tpl2, Tpls...> {
        using Tuple1_orig = typename remove_cvref<Tpl1>::type;
        using Tuple2_orig = typename remove_cvref<Tpl2>::type;

        template<typename... Raw>
        static auto _do(Tpl1&& tp1, Tpl2&& tp2, Tpls&&... tpls, Raw&&... elems) {
            if constexpr(std::is_same_v<Tuple1_orig, Tuple<>>)
            return concater<Ret, std::make_integer_sequence<size_t, length<Tuple2_orig>::get()>, Tpl2, Tpls...>::_do(
                    std::forward<Tpl2>(tp2),
                    std::forward<Tpls>(tpls)...,
                    std::forward<Raw>(elems)...);
            else
                return concater<Ret, std::make_integer_sequence<size_t, length<Tuple2_orig>::get()>, Tpl2, Tpls...>::_do(
                        std::forward<Tpl2>(tp2),
                        std::forward<Tpls>(tpls)...,
                        std::forward<Raw>(elems)...,
                        get<Idx>(std::forward<Tpl1>(tp1))...);
        }
    };

}

template<typename Tpl, typename... Tpls>
decltype(auto) tupleCat(Tpl&& tpl_ft, Tpls&&... tpls)
{
    using Tuple_orig = typename helper::remove_cvref<Tpl>::type;
    return helper::concater<typename helper::merged_tuples<Tuple_orig , typename helper::remove_cvref<Tpls>::type...>::type,
    std::make_integer_sequence<size_t, helper::length<Tuple_orig>::get()>, Tpl, Tpls...>::_do(
            std::forward<Tpl>(tpl_ft), std::forward<Tpls>(tpls)...);
}

template<typename... Types>
Tuple<std::decay_t<Types>...> makeTuple(Types&&... elems) {
    return Tuple<std::decay_t<Types>...>(std::forward<Types>(elems)...);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
