#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

void input_optimisation() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
}

int maximum_weight(const std::vector<int>& a, const int n, const int s) {
    std::vector<std::vector<bool> > dp(n + 1, std::vector<bool>(s + 1, false));
    dp[0][0] = true;
    for (int i = 1; i <= n; ++i)
        for (int j = 0; j <= s; ++j)
            dp[i][j] = (dp[i - 1][j]) | (a[i - 1] <= j && dp[i - 1][j - a[i - 1]]);
    for (int j = s; j >= 0; --j)
        if (dp[n][j])
            return j;
    return 0;    
}
int32_t main() {
    input_optimisation();
    int n, s;
    std::cin >> s >> n;

    std::vector<int> a(n);

    for (int i = 0; i < n; ++i)
        std::cin >> a[i];

    std::cout << maximum_weight(a, n, s);

    return 0;
}