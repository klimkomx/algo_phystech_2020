#include <vector>
#include <iostream>
#include  <algorithm>
#include <utility>

struct cell {
    int min,
        idx;
};

class sparce_table {
    std::vector<std::vector<cell>> body;
    size_t size() const { return body[0].size(); }

public:
    explicit sparce_table(const std::vector<int> &mas);
    int sdmin(size_t l, size_t r) const;
    cell getMin(size_t l, size_t r) const;
};

sparce_table::sparce_table(const std::vector<int> &mas) {
    int n = mas.size();
    body.push_back(std::vector<cell>());
    for (int j = 0; j < n; ++j) {
        body[0].push_back({mas[j], j});
    }
    for (int i = 1; (1 << i) <= n; ++i) {
        body.push_back(std::vector<cell>());
        for (int j = 0; j < n; ++j) {
            if (body[i - 1][j].min <= body[i - 1][std::min(n - 1, j + (1 << (i - 1)))].min)
                body[i].push_back(body[i - 1][j]);
            else
                body[i].push_back(body[i - 1][std::min(n - 1, j + (1 << (i - 1)))]);
        }
    }
}

cell sparce_table::getMin(size_t l, size_t r) const {
    size_t diff = r - l;
    size_t i = -1;
    while (1 << (++i + 1) <= diff);
    cell val1(body[i][l]), val2(body[i][r - (1 << i) + 1]);
    return (val1.min < val2.min) ? val1 : val2;

}

int sparce_table::sdmin(size_t l, size_t r) const {
    cell min = getMin(l, r), cand1, cand2;
//    std::cerr << min.idx + 1 << " " << getMin(l + 1, r).min << "\n";
    if (min.idx == l)
        return getMin(l + 1, r).min;
    else if (min.idx == r)
        return getMin(l, r - 1).min;
    else
        return std::min(getMin(l, min.idx - 1).min, getMin(min.idx + 1, r).min);
}

void solve(const std::vector<int> &mas, const std::vector<std::pair<int, int>> &queries, std::vector<int> &ans) {
    sparce_table table(mas);
    for (auto &p : queries)
        ans.push_back(table.sdmin(p.first, p.second));
}
int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);

    int n, m;
    std::cin >> n >> m;

    std::vector<int> mas(n);
    std::vector<std::pair<int, int>> queries(m);
    for (int i = 0; i < n; ++i)
        std::cin >> mas[i];

    for (auto &p : queries) {
        std::cin >> p.first >> p.second;
        --p.first, --p.second;
    }

    std::vector<int> ans;
    solve(mas, queries, ans);

    for (auto x : ans)
        std::cout << x << "\n";
}