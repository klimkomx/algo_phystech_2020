#include <iostream>
#include <vector>
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

const long long MOD = 1e9 + 7;

inline void input_optimization() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(false);
}

void print(std::vector<std::vector<long long> >& mt) {
    for (int i = 0 ; i < mt.size(); ++i) {
        for (int j = 0; j < mt.size(); ++j)
            std::cout << mt[i][j] << " ";
        std::cout << "\n";
    }
    std::cout << std::endl;
}

void pw(std::vector<std::vector<long long> >& mt, long long pw, int n) {
    std::vector<std::vector<long long> > ans(n, std::vector<long long>(n, 0)),
            copy;
    for (int i = 0; i < n; ++i)
        ans[i][i] = 1;

    while (pw != 0) {
        if (pw & 1) {
            copy = ans;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j) {
                    ans[i][j] = 0;
                    for (int k = 0; k < n; ++k)
                        ans[i][j] = (ans[i][j] + copy[i][k] * mt[k][j]) % MOD;
                }
            --pw;
        } else {
            copy = mt;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < n; ++j) {
                    mt[i][j] = 0;
                    for (int k = 0; k < n; ++k)
                        mt[i][j] = (mt[i][j] + copy[i][k] * copy[k][j]) % MOD;
                }
            pw >>= 1;
        }
    }
    mt = ans;
}

int hamilton_path(std::vector<std::vector<int> >& dp,
                   std::vector<std::vector<int> >& pr,
                   std::vector<std::vector<int> >& dst,
                   int n) {
    for (int i = 0; i < (1 << n); ++i) {
        for (int j = 0; j < n; ++j) {
            if (((i >> j)&1) && (i - (1 << j)) == 0)
                dp[i][j] = 0;
            else if (((i >> j)&1))
                for (int k = 0; k < n; ++k) {
                    if (j == k)
                        continue;
                    if (((i >> k)&1) && dp[i - (1 << j)][k] + dst[k][j] <= dp[i][j]) {
                        dp[i][j] = dp[i - (1 << j)][k] + dst[k][j];
                        pr[i][j] = k;
                    }

                }
        }
    }
    int best = 0;
    for (int i = 1; i < n; ++i)
        if (dp[(1 << n) - 1][best] > dp[(1 << n) - 1][i])
            best = i;
    return best;
}


int32_t main() {
    input_optimization();
    int n;

    std::cin >> n;

    std::vector<std::vector<int> > d(n, std::vector<int>(n)),
            dp((1 << n), std::vector<int>(n, INT32_MAX)),
            pr((1 << n), std::vector<int>(n, -1));
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            std::cin >> d[i][j];

    auto best = hamilton_path(dp, pr, d, n);


    std::cout << dp[(1 << n) - 1][best] << std::endl;

    int mask = (1 << n) - 1, nmask;
    while (mask != 0) {
        std::cout << best + 1 << " ";
        nmask = mask - (1 << best);
        best = pr[mask][best];
        mask = nmask;
    }
    return 0;
}