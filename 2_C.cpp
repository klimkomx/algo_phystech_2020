#include <iostream>
#include <vector>

int myRand(int a, int b)
{
    return ((std::rand() << 10) + std::rand()) % (b - a - 1) + a + 1;
}
template<typename T>
std::pair<int, int> partition(std::vector<T>& mas, int leftB, int rightB)
{
    int md = myRand(leftB, rightB);
    std::swap(mas[md], mas[leftB + 1]);
    if (mas[leftB] > mas[leftB + 1])
        std::swap(mas[leftB], mas[leftB + 1]);
    if (mas[leftB + 1] < mas[rightB])
        std::swap(mas[leftB + 1], mas[rightB]);
    if (mas[leftB] > mas[rightB])
        std::swap(mas[leftB], mas[rightB]);
    T pivot = mas[rightB];
    int lb = leftB, rb;
    for (int i = leftB; i < rightB; ++i)
    {
        if (mas[i] < pivot)
        {
            std::swap(mas[lb], mas[i]);
            ++lb;
        }
    }
    std::swap(mas[lb], mas[rightB]);
    rb = lb;
    while (rb < rightB && mas[rb] == mas[rb + 1])
        ++rb;
    return {lb, rb};
}

template <typename T>
T findKthOrderedStatistics(std::vector<T>& mas, int lB, int rB, int k)
{
    std::pair<int, int> mid;
    while (lB <= rB)
    {
        if (lB + 1 >= rB)
        {
            if (rB == lB + 1 && mas[rB] < mas[lB])
                std::swap(mas[lB], mas[rB]);
            return mas[k];
        }
        mid = partition(mas, lB, rB);
        if (mid.first <= k && k <= mid.second)
            return mas[k];
        else if (mid.first > k)
            rB = mid.first - 1;
        else
            lB = mid.second + 1;
    }
}

int main()
{
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);
    
    int n, k;

    std::cin >> n >> k;
    
    std::vector<int> a(n);
    
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
    }

    int ans = findKthOrderedStatistics<int>(a, 0, a.size() - 1,  k);
    
    std::cout << ans;
    
    return 0;
}