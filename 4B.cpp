#include <iostream>
#include <vector>

class Fenwick {
private:
    std::vector<int> original;
    std::vector<int> tree;
    int size;
    
    void add(int i, int plus);
public:
    Fenwick(int s);
    Fenwick(std::vector<int>& a);

    void set(int i, int x);
    
    int sum(int i) const;
    inline int sum(int l, int r) const;
};

Fenwick::Fenwick(int s) {
    size = s;
    tree.resize(s, 0);
    original.resize(s, 0);
}
Fenwick::Fenwick(std::vector<int>& a) {
    original = a;
    size = a.size();
    tree.resize(size, 0);
    for (int i = 0; i < size; ++i) {
        add(i, original[i]);
    }
}

void Fenwick::add(int i, int plus) {
    while (i < size) {
        tree[i] += plus;
        i |= (i + 1);
    }
}
void Fenwick::set(int i, int x) {
    add(i, x - original[i]);
    original[i] = x;
}

int Fenwick::sum(int i) const{
    int ans = 0;
    while (i >= 0) {
        ans += tree[i];
        i = (i & (i + 1)) - 1;
    }
    return ans;
}
int Fenwick::sum(int l, int r) const {
    return sum(std::min(r, size - 1)) - sum(l - 1);
}

class Solve {
private:
    Fenwick odd, even;
public:
    Solve(std::vector<int>& a);
    void set(int i, int c);

    int sum(int l, int r);
};

Solve::Solve(std::vector<int>& a):
odd(a.size() / 2), 
even(a.size() - a.size()/2) {
    for (int i = 0; i < a.size(); ++i) {
        if (i % 2 == 0) {
            even.set(i / 2, a[i]);
        } else {
            odd.set(i / 2, a[i]);
        }
    }
}

void Solve::set(int i, int c) {
    if (i % 2 == 0) {
        even.set(i / 2, c);
    } else {
        odd.set(i / 2, c);
    }
}
int Solve::sum(int l, int r) {
    int le, lo, re, ro;
    if (l % 2 == 0) {
        le = lo = l/2;
    } else {
        lo = l/2;
        le = lo + 1;
    }
    if (r % 2 == 1) {
        re = ro = r/2;
    } else {
        ro = r/2 - 1;
        re = r/2;
    }
    return (even.sum(le, re) - odd.sum(lo, ro)) *
           ((le == lo) ? 1 : -1);
}

int main() {
    int n, m, com, x, y;

    std::cin >> n;

    std::vector<int> a(n);
    for (int i = 0; i < n; ++i)
        std::cin >> a[i];
    Solve signedSum(a);
    std::cin >> m;

    for (int i = 0; i < m; ++i) {
        std::cin >> com >> x >> y;
        if (com == 1) {
            std::cout << signedSum.sum(x - 1, y - 1) << "\n";
        } else {
            signedSum.set(x - 1, y);
        }
    }
    return 0;
}