#include<vector>
#include<stack>
#include<iostream>
#include <queue>
#include <algorithm>
#include <map>
using std::vector;

struct IGraph {
    virtual ~IGraph() = default;

    virtual void addEdge( int from, int to ) = 0;
    virtual void deleteEdge(int from, int to) = 0;
    virtual vector<int> getNextVerticies( int from ) const = 0;
    virtual bool hasEdge( int from, int to ) const = 0;
    virtual size_t size() const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph( size_t vertexCount );
    ListGraph( IGraph* graph );
    virtual ~ListGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<int>> lists;
};
ListGraph::ListGraph(size_t vertexCount): lists(vector<vector<int> >(vertexCount)) {

}
ListGraph::ListGraph( IGraph* graph ) :
        ListGraph( graph->size() ) {
    for( int i = 0; i < lists.size(); ++i )
        lists[i] = graph->getNextVerticies( i );
}
void ListGraph::addEdge(int from, int to) {
    lists[from].push_back( to );
}
void ListGraph::deleteEdge(int from, int to) {
    int pos = -1;
    for (int i = 0; i < lists[from].size(); ++i) {
        if (lists[from][i] == to) {
            pos = i;
            break;
        }
    }
    if (pos != -1 && pos != lists[from].size() - 1)
        std::swap(lists[from][pos], lists[from][lists[from].size() - 1]);
    if (pos != -1)
        lists[from].pop_back();
}
vector<int> ListGraph::getNextVerticies( int from ) const {
    return lists[from];
}
bool ListGraph::hasEdge(int from, int to) const {
    for ( int i = 0; i < lists[from].size(); ++i ) {
        if (lists[from][i] == to)
            return true;
    }
    return false;
}
size_t ListGraph::size() const {
    return lists.size();
}

class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(size_t vertexCount);
    MatrixGraph( IGraph* graph );
    virtual ~MatrixGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from ) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<bool>> matrix;
};
MatrixGraph::MatrixGraph(size_t vertexCount): matrix(vector<vector<bool> >(vertexCount,vector<bool>(vertexCount, false))) {}
MatrixGraph::MatrixGraph(IGraph *graph): MatrixGraph(graph->size()) {
    for (int i = 0; i < graph -> size(); ++i) {
        vector<int> list = graph -> getNextVerticies(i);
        for (auto j : list) {
            matrix[i][j] = true;
        }
    }
}
void MatrixGraph::addEdge( int from, int to ) {
    matrix[from][to] = true;
}
void MatrixGraph::deleteEdge(int from, int to) {
    matrix[from][to] = false;
}
vector<int> MatrixGraph::getNextVerticies(int from) const {
    vector<int> res;
    for (int i = 0; i < size(); ++i) {
        if (matrix[from][i])
            res.push_back(i);
    }
    return res;
}
bool MatrixGraph::hasEdge(int from, int to) const {
    return matrix[from][to];
}
size_t MatrixGraph::size() const {
    return matrix.size();
}

class PairGraph : public IGraph {
public:
    explicit PairGraph(size_t vertexCount);
    PairGraph( IGraph* graph );
    virtual ~PairGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<std::pair<int, int> > edges;
    size_t graph_size;
};
PairGraph::PairGraph(size_t vertexCount): edges(), graph_size(vertexCount){}
PairGraph::PairGraph(IGraph *graph): PairGraph(graph -> size()) {
    int n = graph -> size();
    vector<int> vt;
    for (int  = 0; i < n; ++i) {
        vt = graph -> getNextVerticies(i);
        for (int j = 0; j < vt.size(); ++i) {
            edges.push_back({i, j});
        }
    }
}
void PairGraph::addEdge(int from, int to) {
    edges.push_back({from, to});
}
void PairGraph::deleteEdge(int from, int to) {
    auto it = edges.begin();
    for(; it != edges.end(); ++it) {
        if (it -> first == from && it -> second == to) {
            edges.erase(it);
            return;
        }
    }
}
vector<int> PairGraph::getNextVerticies(int from) const {
    vector<int> ans;
    for (int i = 0; i  < edges.size(); ++i) {
        if (edges[i].first == from)
            ans.push_back(edges[i].second);
    }
    return ans;
}
bool PairGraph::hasEdge(int from, int to) const {
    for (const auto & edge : edges)
        if (from == edge.first && to == edge.second)
            return true;
    return false;
}
size_t PairGraph::size() const {
    return graph_size;
}

int dfs(IGraph& gr, vector<int>& ans, vector<int>& color, int u) {
    color[u] = 1;
    auto next = gr.getNextVerticies(u);
    for (auto v : next) {
        if (color[v] == 1) {
            ans.push_back(u);
            return v;
        } else if (color[v] == 0) {
            int res = dfs(gr, ans, color, v);
            if (res != -1) {
                ans.push_back(u);
                return res;
            }
        }
    }
    color[u] = 2;
    return -1;
}

vector<int> findCycle(IGraph& gr) {
    vector<int> color(gr.size(),  0);
    vector<int> path, ans;
    int res;
    for (int i = 0; i < gr.size(); ++i) {
        if (color[i] == 0) {
            res = dfs(gr, path, color, i);
            if (res != -1) {
                int k = 0;
                while (path[k] != res)
                    ans.push_back(path[k++] + 1);
                ans.push_back(res + 1);
                std::reverse(ans.begin(), ans.end());
                return ans;
            }
        }
    }
    return ans;
}

int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);
    int n, m, a, b;
    std::cin >> n >> m;
    ListGraph gr(n);

    for (int i = 1; i <= m; ++i) {
        std::cin >> a >> b;
        --a;
        --b;
        gr.addEdge(a, b);
    }

    auto ans = findCycle(gr);

    if (ans.empty())
        std::cout << "NO";
    else {
        std::cout << "YES\n";
        for (auto v : ans) {
            std::cout << v << " ";
        }
    }
    return 0;
}
