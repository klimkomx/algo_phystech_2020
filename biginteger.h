#include <string>
#include <vector>

class BigInteger
{
private:
    std::vector<int> number;
    bool sign = true; // true - >= 0, false - < 0

public:
    BigInteger();
    ~BigInteger() = default;
    BigInteger(const BigInteger& a);
    BigInteger(int a);
    BigInteger(const std::string& s);

    std::string toString() const;
    explicit operator bool();

    BigInteger abs() const;

    const BigInteger operator-();
    BigInteger& operator++();
    BigInteger& operator--();

    const BigInteger operator++(int);
    const BigInteger operator--(int);

    BigInteger& operator*=(const BigInteger &b);
    BigInteger& operator+=(const BigInteger &b);
    BigInteger& operator-=(const BigInteger &b);
    BigInteger& operator%=(const BigInteger &b);
    BigInteger& operator/=(const BigInteger &b);

    friend BigInteger operator+(const BigInteger &a, const BigInteger &b);
    friend BigInteger operator-(const BigInteger &a, const BigInteger &b);
    friend BigInteger operator*(const BigInteger &a, const BigInteger &b);
    friend BigInteger operator/(const BigInteger &a, const BigInteger &b);
    friend BigInteger operator%(const BigInteger &a, const BigInteger &b);

    friend bool operator==(const BigInteger &a, const BigInteger &b);
    friend bool operator!=(const BigInteger &a, const BigInteger &b);
    friend bool operator>(const BigInteger &a, const BigInteger &b);
    friend bool operator<(const BigInteger &a, const BigInteger &b);
    friend bool operator>=(const BigInteger &a, const BigInteger &b);
    friend bool operator<=(const BigInteger &a, const BigInteger &b);

    friend std::istream &operator>>(std::istream &in, BigInteger &a);
private:
    static void module_sum(BigInteger& a, const BigInteger& b);
    static void module_dif(BigInteger& a, const BigInteger& b);
    static int compare_by_module(const BigInteger& a, const BigInteger& b);
    static int compare(const BigInteger& a, const BigInteger& b); // -1 - <, 0 - ==, 1 - >

    void shrink_zeros();
    void add_radix();
};

BigInteger::BigInteger() {
    sign = true;
}

BigInteger::BigInteger(const BigInteger& a) {
    number = a.number;
    sign = a.sign;
}

BigInteger::BigInteger(int a) {
    if (a < 0) {
        a *= -1;
        sign = false;
    } else {
        sign = true;
        if (a == 0) {
            number.push_back(0);
        }
    }
    while (a != 0) {
        number.push_back(a % 10);
        a /= 10;
    }
}

BigInteger::BigInteger(const std::string& s) {
    sign = true;
    for (int it = (int)s.size() - 1; it > 0; --it) {
        number.push_back(s[it] - '0');
    }
    if (s[0] == '-') {
        sign = false;
    } else if (s[0] != '+') {
        number.push_back(s[0] - '0');
    }
}

std::string BigInteger::toString() const {
    std::string res = sign ? "" : "-";
    int it = (int)number.size() - 1;
    while (number[it] == 0)
        --it;
    for (; it >= 0; --it) {
        res.push_back(number[it] + '0');
    }
    if (res.empty() || (res.size() == 1 && res[0] == '-')  )
        res = '0';
    return res;
}

BigInteger operator-(const BigInteger &a, const BigInteger &b) {
    BigInteger res = a;
    return  res -= b;
}
BigInteger operator+(const BigInteger &a, const BigInteger &b) {
    BigInteger res = a;
    return res += b;
}
BigInteger operator*(const BigInteger &a, const BigInteger &b) {
    BigInteger res = a;
    return res *= b;
}
BigInteger operator/(const BigInteger &a, const BigInteger &b) {
    BigInteger res = a;
    return res /= b;
}
BigInteger operator%(const BigInteger &a, const BigInteger &b) {
    BigInteger res = a;
    return res %= b;
}

const BigInteger BigInteger::operator-() {
    BigInteger res = *this;
    if (res.number.size() == 1 && number[0] == 0) {
        res.number[0] = 0;
        res.sign = true;
    } else
        res.sign = !res.sign;
    return res;
}
BigInteger& BigInteger::operator++() {
    *this += 1;
    return *this;
}
BigInteger& BigInteger::operator--() {
    *this -= 1;
    return *this;
}
const BigInteger BigInteger::operator++(int) {
    BigInteger tmp = *this;
    ++(*this);
    return tmp;
}
const BigInteger BigInteger::operator--(int) {
    BigInteger tmp = *this;
    --(*this);
    return tmp;
}

BigInteger& BigInteger::operator+=(const BigInteger &b) {
    if ((sign && b.sign) || (!sign && !b.sign))  {
        module_sum(*this, b);
        return *this;
    }
    int compByMod = compare_by_module(*this, b);
    if (compByMod == -1) {
        BigInteger tmp = b;
        module_dif(tmp, *this);
        *this = tmp;
        sign = b.sign;
    } else if (compByMod == 1) {
        module_dif(*this, b);
    } else
        *this = 0;
    return *this;
}
BigInteger& BigInteger::operator-=(const BigInteger &b) {
    if ((sign && !b.sign) || (!sign && b.sign)) {
        module_sum(*this, b);
        return *this;
    }
    int compByMod = compare_by_module(*this, b);
    if (compByMod == -1) {
        BigInteger tmp = b;
        module_dif(tmp, *this);
        *this = tmp;
        sign = !b.sign;
    } else if (compByMod == 1) {
        module_dif(*this, b);
    } else
        *this = 0;
    return *this;
}
BigInteger& BigInteger::operator*=(const BigInteger &b) {
    BigInteger res;
    int ft_s = (*this).number.size(),
        sd_s = b.number.size();
    res.number.resize((ft_s + sd_s + 1));
    for (int i = 0; i <= ft_s + sd_s; ++i) {
        res.number[i] = 0;
    }
    res.sign = (*this).sign == b.sign;
    for (int i = 0; i < ft_s; ++i) {
        for (int j = 0; j < sd_s; ++j) {
            res.number[i + j] += (*this).number[i] * b.number[j];
        }
    }
    for (int i = 0; i < ft_s + sd_s; ++i) {
        res.number[i + 1] += res.number[i] / 10;
        res.number[i] %= 10;
    }
    res.number[ft_s + sd_s] %= 10;
    res.shrink_zeros();
    *this = res;
    return *this;
}
BigInteger& BigInteger::operator/=(const BigInteger &b) {
    BigInteger res, cur;
    res.number.resize((*this).number.size());
    res.sign = (*this).sign == b.sign;
    for (int i = (int)(*this).number.size() - 1; i >= 0; --i) {
        cur.add_radix();
        cur.shrink_zeros();
        cur.number[0] = (*this).number[i];
        int l = 0, r = 10, m, last = 0;
        while (l < r) {
            m = (l + r) / 2;
            BigInteger tmp = b * m;
            if (0 >= compare_by_module(tmp, cur)) {
                last = m;
                l = m + 1;
            } else
                r = m;
        }
        res.number[i] = last;
        cur -= b.abs() * last;
    }
    res.shrink_zeros();
    *this = res;
    return *this;
}
BigInteger& BigInteger::operator%=(const BigInteger &b) {
    BigInteger tmp = (*this) / b, res;
    res = (*this).abs() - b.abs() * tmp.abs();
    res.sign = (*this).sign == b.sign;
    *this = res;
    return *this;
}

bool operator==(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) == 0;
}
bool operator!=(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) != 0;
}
bool operator>(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) == 1;
}
bool operator<(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) == -1;
}
bool operator>=(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) >= 0;
}
bool operator<=(const BigInteger &a, const BigInteger &b) {
    return BigInteger::compare(a, b) <= 0;
}

int BigInteger::compare(const BigInteger &a, const BigInteger &b) {
    if (a.sign && !b.sign)
        return 1;
    if (b.sign && !a.sign)
        return -1;
    if (!a.sign && !b.sign)
        return -compare_by_module(a, b);
    return compare_by_module(a, b);
}
int BigInteger::compare_by_module(const BigInteger &a, const BigInteger &b) {
    if (a.number.size() < b.number.size()) return -1;
    if (a.number.size() > b.number.size()) return 1;
    for (int i = (int)a.number.size() - 1; i >= 0; --i) {
        if (a.number[i] < b.number[i]) return -1;
        if (a.number[i] > b.number[i]) return 1;
    }
    return 0;
}

void BigInteger::module_sum(BigInteger &a, const BigInteger &b) {
    int addition = 0;
    for (size_t i = 0; (i < b.number.size()) || addition; ++i) {
        if (i == a.number.size())
            a.number.push_back(0);
        a.number[i] += ((i < b.number.size()) ? b.number[i] : 0) + addition;
        addition = a.number[i] / 10;
        a.number[i] %= 10;
    }
    a.shrink_zeros();
}

void BigInteger::module_dif(BigInteger &a, const BigInteger &b) {
    int minus = 0;
    for (size_t i = 0; (i < a.number.size()) || minus; ++i) {
        a.number[i] -= ((i < b.number.size()) ? b.number[i] : 0) + minus;
        if (a.number[i] >= 0)
            minus = 0;
        else {
            a.number[i] += 10;
            minus = 1;
        }
    }
    a.shrink_zeros();
}

void BigInteger::add_radix() {
    std::vector<int> res(number.size() + 1, 0);
    for (size_t i = 1; i <= number.size(); ++i) {
        res[i] = number[i - 1];
    }
    number = res;
}
void BigInteger::shrink_zeros() {
    while (!number.empty() && number[number.size() - 1] == 0)
        number.pop_back();
    if (number.empty()) {
        sign = true;
        number.push_back(0);
    }
}

BigInteger::operator bool() {
    return (*this) != 0;
}
BigInteger BigInteger::abs() const {
    BigInteger res = *this;
    res.sign = true;
    return res;
}

std::istream &operator>>(std::istream& in, BigInteger& a) {
    std::string str;
    in >> str;
    a = BigInteger(str);
    return in;
}
std::ostream  &operator<<(std::ostream& out, const BigInteger& a) {
    out << a.toString();
    return out;
}

//RATIONAL

class Rational {
private:
    BigInteger numerator,
               denominator;
public:
    Rational();
    Rational(const BigInteger& a);
    Rational(const int a);

    std::string toString();
    std::string  asDecimal(size_t precision);
    explicit operator double();

    const Rational operator-();
    Rational &operator*=(const Rational &b);
    Rational &operator+=(const Rational &b);
    Rational &operator-=(const Rational &b);
    Rational &operator/=(const Rational &b);

    friend bool operator==(const Rational &a, const Rational &b);
    friend bool operator!=(const Rational &a, const Rational &b);
    friend bool operator<(const Rational &a, const Rational &b);
    friend bool operator>(const Rational &a, const Rational &b);
    friend bool operator<=(const Rational &a, const Rational &b);
    friend bool operator>=(const Rational &a, const Rational &b);
private:
    void reduce();
    BigInteger static _gcd(const BigInteger& a, const BigInteger& b);
};

Rational::Rational():
numerator(0),
denominator(1)
{}
Rational::Rational(const BigInteger& a):
numerator(a),
denominator(1)
{}
Rational::Rational(const int a):
numerator(a),
denominator(1)
{}

BigInteger Rational::_gcd(const BigInteger &a, const BigInteger &b) {
    BigInteger a_c = a.abs();
    BigInteger b_c = b.abs();
    while (b_c != 0 && a_c != 0) {
        if (a_c > b_c)
            a_c %= b_c;
        else
            b_c %= a_c;
    }
    return (a_c + b_c);
}

void Rational::reduce() {
    if (denominator < 0) {
        numerator = -numerator;
        denominator = denominator.abs();
    }
    BigInteger gcd = _gcd(numerator, denominator);
    numerator /= gcd;
    denominator /= gcd;
}

Rational& Rational::operator*=(const Rational &b) {
    numerator *= b.numerator;
    denominator *= b.denominator;
    reduce();
    return *this;
}
Rational& Rational::operator/=(const Rational &b) {
    numerator *= b.denominator;
    denominator *= b.numerator;
    reduce();
    return *this;
}
Rational& Rational::operator+=(const Rational &b) {
    numerator *= b.denominator;
    numerator += b.numerator * denominator;
    denominator *= b.denominator;
    reduce();
    return *this;
}
Rational& Rational::operator-=(const Rational &b) {
    numerator *= b.denominator;
    numerator -= b.numerator * denominator;
    denominator *= b.denominator;
    reduce();
    return *this;
}

Rational operator-(const Rational &a, const Rational &b) {
    Rational res = a;
    return  res -= b;
}
Rational operator+(const Rational &a, const Rational &b) {
    Rational res = a;
    return res += b;
}
Rational operator*(const Rational &a, const Rational &b) {
    Rational res = a;
    return res *= b;
}
Rational operator/(const Rational &a, const Rational &b) {
    Rational res = a;
    return res /= b;
}

bool operator==(const Rational &a, const Rational &b){
    return (a.numerator == b.numerator && a.denominator == b.denominator);
}
bool operator!=(const Rational &a, const Rational &b){
    return (a.numerator != b.numerator && a.denominator != b.denominator);
}
bool operator>(const Rational &a, const Rational &b){
    return (a.numerator * b.denominator > a.denominator * b.numerator);
}
bool operator<(const Rational &a, const Rational &b){
    return (a.numerator * b.denominator < a.denominator * b.numerator);
}
bool operator>=(const Rational &a, const Rational &b){
    return (a.numerator * b.denominator >= a.denominator * b.numerator);
}
bool operator<=(const Rational &a, const Rational &b){
    return (a.numerator * b.denominator <= a.denominator * b.numerator);
}


std::string Rational::toString() {
    if (denominator == 1)
        return numerator.toString();
    return numerator.toString() + "/" + denominator.toString();
}

const Rational Rational::operator-() {
    Rational res = *this;
    res.numerator = -res.numerator;
    return res;
}

std::string  Rational::asDecimal(size_t precision = 0) {
    std::string res, tt;
    BigInteger cur = numerator / denominator,
               copy = numerator.abs();
    res = cur.toString();
    if (precision != 0)
        res.push_back('.');
    else
        return res;
    for (size_t i = 0; i < precision; ++i) {
        cur *= 10;
        copy *= 10;
    }
    BigInteger tmp = copy / denominator - cur;
    if (numerator < 0 && res[0] != '-')
        res = '-' + res;

    tt = tmp.toString();
    if (precision >  tt.size())
    for (size_t i = 0; i < precision - tt.size(); ++i)
        res.push_back('0');
    return res + tmp.toString();
}
Rational::operator double() {
    std::string res = asDecimal(20);
    return std::stod(res);
} 
