 #include <iostream>

template <typename T>
void swap(T& _t1, T& _t2)
{
    T tmp(_t1);
    _t1 = _t2;
    _t2 = tmp;
}

void radixSort(unsigned long long* massive, int size,  const int& positions = 8)
{
    unsigned long long* tmp = new unsigned long long[size];
    unsigned long long cnt[256];
    for (int pos  = 0; pos < positions; ++pos)
    {
        for (int i = 0; i < 256; ++i)
            cnt[i] = 0ull;
        for (int i = 0; i < size; ++i)
        {
            ++cnt[((massive[i] >> pos * 8ull) & 255ull)];
        }
        for (int i = 1; i < 256; ++i)
        {
            cnt[i] += cnt[i - 1];
        }
        for (int i = size - 1; i >= 0; --i)
        {
            tmp[--cnt[((massive[i] >> pos * 8ull) & 255ull)]] = massive[i];
        }
        swap(massive, tmp);
    }
    delete[] tmp;
}

int main()
{
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);
    int n;

    std::cin >> n;

    unsigned long long* massive = new unsigned long long[n];
    for (int i = 0; i < n; ++i)
    {
        std::cin >> massive[i];
    }
    radixSort(massive, n);

    for (int i = 0; i < n; ++i)
    {
        std::cout << massive[i] << " ";
    }

    delete[] massive;
    return 0;
}