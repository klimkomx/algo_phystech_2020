#include <iostream>
#include <vector>
#include <string>
#include <queue>

using std::vector;
using std::queue;
using std::cin;
using std::cout;

struct node {
    int v, w;
    node(int b, int c): v(b), w(c){}
};

void bfs_0_k(vector<vector<node> >& graph, vector<int>& dist, int k, int start) {
    vector<queue<int> > bfs(k + 1);
    vector<bool> used(graph.size(), false);
    bfs[0].push(start);
    dist[start] = 0;
    int num = 1, pos = 0, now;
    while (num > 0) {
        while (bfs[pos % (k + 1)].empty())
            ++pos;
        now = bfs[pos % (k + 1)].front();
        bfs[pos % (k + 1)].pop();
        --num;
        if (!used[now]) {
            used[now] = true;
            for (node nd : graph[now]) {
                if (dist[now] + nd.w< dist[nd.v]) {
                    dist[nd.v] = dist[now] + nd.w;
                    bfs[dist[nd.v] % (k + 1)].push(nd.v);
                    ++num;
                }
            }
        }
    }

}


int main() {
    int n, m, s, t, b, e, w, k = 0;
    cin >> n >> m >> s >> t;
    --s, --t;
    vector<vector<node> > graph(n);
    for (int i = 0; i < m; ++i) {
        cin >> b >> e >> w;
        graph[--b].push_back(node(--e, w));
        graph[e].push_back(node(b, w));
        k = std::max(k, w);
    }
    vector<int> dst(n, INT32_MAX);

    bfs_0_k(graph, dst, k, s);
    
    if (dst[t] == INT32_MAX)
        cout << -1 << std::endl;
    else
        cout << dst[t] << std::endl;
    return 0;
}