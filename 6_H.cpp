#include <iostream>
#include <vector>

inline void input_optimisation() {
    std::ios::sync_with_stdio(0);
    std::cin.tie(0);
}

int lcis(std::vector<int>& ft, std::vector<int>& sd, int n, int m) {

    std::vector<std::vector<int>> dp(n + 1, std::vector<int>(m + 1, 0));

    int prefbestval;
    for (int i = 1; i <= n; ++i) {
        prefbestval = 0;
        for (int j = 1; j <= m; ++j) {
            dp[i][j] = dp[i - 1][j];
            if (ft[i - 1] == sd[j - 1]) dp[i][j] = std::max(prefbestval + 1, dp[i][j]);
            else if (ft[i - 1] > sd[j - 1]) prefbestval = std::max(prefbestval, dp[i - 1][j]);
        }
    }

    int ans = INT32_MIN;
    for (int i = 1; i <= m; ++i) {
        ans = std::max(ans, dp[n][i]);
    }
    return ans;
}

int32_t main() {
    input_optimisation();
    int n, m;
    std::cin >> n >> m;

    std::vector<int> ft(n), sd(m);

    for (int i = 0; i < n; ++i)
        std::cin >> ft[i];
    for (int i = 0; i < m; ++i)
        std::cin >> sd[i];

    std::cout << lcis(ft, sd, n, m);
    return 0;
}