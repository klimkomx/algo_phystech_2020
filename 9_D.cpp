#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::cin;
using std::cout;

struct edge {
    int u, v;
    long long w;
    edge(int a, int b, long long c): u(a - 1), v(b - 1), w(c){}
};

void dfs_find_all_reachble(int u, vector<vector<int> >& graph, vector<bool>& used) {
    used[u] = true;
    for (auto v : graph[u]) {
        if (!used[v])
            dfs_find_all_reachble(v, graph, used);
    }
}

void ford_bellman(vector<edge>& graph,
                  vector<vector<int> >& graph_dfs,
                  vector<long long>& dist,
                  vector<bool>& updated,
                  int m,
                  int n,
                  int s) {
    dist[s] = 0;

    for (int i = 0; i < n - 1; ++i) {
        for (int j = 0; j < m; ++j) {
            if (dist[graph[j].u] < INT64_MAX) {
                dist[graph[j].v] = std::min(dist[graph[j].v], dist[graph[j].u] + graph[j].w);
            }
        }
    }

    for (int j = 0; j < m; ++j) {
        if (dist[graph[j].u] < INT64_MAX && dist[graph[j].u] + graph[j].w < dist[graph[j].v]) {
            dist[graph[j].v] = dist[graph[j].u] + graph[j].w;
            dfs_find_all_reachble(graph[j].v, graph_dfs, updated);
        }
    }
}


int main() {
    int n, m, s, x, y;
    long long z;
    cin >> n >> m >> s;
    --s;

    vector<edge> g;
    vector<vector<int> > gr(n);
    for (int i = 0; i < m; ++i) {
        cin >> x >> y >> z;
        g.push_back(edge(x, y, z));
        gr[x - 1].push_back(y - 1);
    }
    vector<long long> dst(n, INT64_MAX);
    vector<bool> updated_on_last_iteration(n, false);

    ford_bellman(g, gr, dst, updated_on_last_iteration, m, n, s);

    for (int i = 0; i < n; ++i) {
        if (updated_on_last_iteration[i])
            cout << "-\n";
        else if (dst[i] == INT64_MAX)
            cout << "*\n";
        else
            cout << dst[i] << "\n";
    }

    return 0;
}