#include<vector>
#include<stack>
#include<iostream>
#include <queue>
#include <algorithm>
#include <map>
using std::vector;

struct IGraph {
    virtual ~IGraph() = default;

    virtual void addEdge( int from, int to ) = 0;
    virtual void deleteEdge(int from, int to) = 0;
    virtual vector<int> getNextVerticies( int from ) const = 0;
    virtual bool hasEdge( int from, int to ) const = 0;
    virtual size_t size() const = 0;
};

class ListGraph : public IGraph {
public:
    explicit ListGraph( size_t vertexCount );
    ListGraph( IGraph* graph );
    virtual ~ListGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<int>> lists;
};
ListGraph::ListGraph(size_t vertexCount): lists(vector<vector<int> >(vertexCount)) {

}
ListGraph::ListGraph( IGraph* graph ) :
        ListGraph( graph->size() ) {
    for( int i = 0; i < lists.size(); ++i )
        lists[i] = graph->getNextVerticies( i );
}
void ListGraph::addEdge(int from, int to) {
    lists[from].push_back( to );
}
void ListGraph::deleteEdge(int from, int to) {
    int pos = -1;
    for (int i = 0; i < lists[from].size(); ++i) {
        if (lists[from][i] == to) {
            pos = i;
            break;
        }
    }
    if (pos != -1 && pos != lists[from].size() - 1)
        std::swap(lists[from][pos], lists[from][lists[from].size() - 1]);
    if (pos != -1)
        lists[from].pop_back();
}
vector<int> ListGraph::getNextVerticies( int from ) const {
    return lists[from];
}
bool ListGraph::hasEdge(int from, int to) const {
    for ( int i = 0; i < lists[from].size(); ++i ) {
        if (lists[from][i] == to)
            return true;
    }
    return false;
}
size_t ListGraph::size() const {
    return lists.size();
}

class MatrixGraph : public IGraph {
public:
    explicit MatrixGraph(size_t vertexCount);
    MatrixGraph( IGraph* graph );
    virtual ~MatrixGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from ) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<vector<bool>> matrix;
};
MatrixGraph::MatrixGraph(size_t vertexCount): matrix(vector<vector<bool> >(vertexCount,vector<bool>(vertexCount, false))) {}
MatrixGraph::MatrixGraph(IGraph *graph): MatrixGraph(graph->size()) {
    for (int i = 0; i < graph -> size(); ++i) {
        vector<int> list = graph -> getNextVerticies(i);
        for (auto j : list) {
            matrix[i][j] = true;
        }
    }
}
void MatrixGraph::addEdge( int from, int to ) {
    matrix[from][to] = true;
}
void MatrixGraph::deleteEdge(int from, int to) {
    matrix[from][to] = false;
}
vector<int> MatrixGraph::getNextVerticies(int from) const {
    vector<int> res;
    for (int i = 0; i < size(); ++i) {
        if (matrix[from][i])
            res.push_back(i);
    }
    return res;
}
bool MatrixGraph::hasEdge(int from, int to) const {
    return matrix[from][to];
}
size_t MatrixGraph::size() const {
    return matrix.size();
}

class PairGraph : public IGraph {
public:
    explicit PairGraph(size_t vertexCount);
    PairGraph( IGraph* graph );
    virtual ~PairGraph() override {}

    virtual void addEdge( int from, int to ) override;
    virtual void deleteEdge( int from, int to ) override;
    virtual vector<int> getNextVerticies( int from) const override;
    virtual bool hasEdge( int from, int to ) const override;
    virtual size_t size() const override;
private:
    vector<std::pair<int, int> > edges;
    size_t graph_size;
};
PairGraph::PairGraph(size_t vertexCount): edges(), graph_size(vertexCount){}
PairGraph::PairGraph(IGraph *graph): PairGraph(graph -> size()) {
    int n = graph -> size();
    vector<int> vt;
    for (int  = 0; i < n; ++i) {
        vt = graph -> getNextVerticies(i);
        for (int j = 0; j < vt.size(); ++i) {
            edges.push_back({i, j});
        }
    }
}
void PairGraph::addEdge(int from, int to) {
    edges.push_back({from, to});
}
void PairGraph::deleteEdge(int from, int to) {
    auto it = edges.begin();
    for(; it != edges.end(); ++it) {
        if (it -> first == from && it -> second == to) {
            edges.erase(it);
            return;
        }
    }
}
vector<int> PairGraph::getNextVerticies(int from) const {
    vector<int> ans;
    for (int i = 0; i  < edges.size(); ++i) {
        if (edges[i].first == from)
            ans.push_back(edges[i].second);
    }
    return ans;
}
bool PairGraph::hasEdge(int from, int to) const {
    for (const auto & edge : edges)
        if (from == edge.first && to == edge.second)
            return true;
    return false;
}
size_t PairGraph::size() const {
    return graph_size;
}

void dfsInOut(IGraph& gr, vector<bool>& used, vector<int>& in, vector<int>& out, int u, int& cnt) {
    used[u] = true;
    in[u] = ++cnt;

    auto next = gr.getNextVerticies(u);

    for (auto v : next) {
        if (!used[v]) {
            dfsInOut(gr, used, in, out, v, cnt);
        }
    }
    out[u] = ++cnt;
}

void findInOut(IGraph& gr, vector<int>& in, vector<int>& out, int ft) {
    vector<bool> used(gr.size(), false);
    int cnt = 0;
    dfsInOut(gr, used, in, out, ft, cnt);
}

int main() {
    std::cin.tie(0);
    std::ios::sync_with_stdio(0);
    int n, m, main, a;
    std::cin >> n;

    ListGraph gr(n);

    for (int i = 0; i < n; ++i) {
        std::cin >> a;
        --a;
        if (a == -1)
            main = i;
        else
            gr.addEdge(a, i);
    }

    vector<int> in(n), out(n);
    findInOut(gr, in, out, main);


    std::cin >> m;
    
    int ft, sd;
    for (int i = 0; i < m; ++i) {
        std::cin >> ft >> sd;
        --ft, --sd;
        std::cout << static_cast<int>(in[ft] < in[sd] && out[ft] > out[sd]) << "\n";
    }
    return 0;
}
