 #include <iostream>

template <typename T>
class vector
{
public:
    vector();
    vector(int defSize);
    ~vector();
    vector(const vector<T>& otherVector);


    vector& operator= (const vector<T>& otherVector);
    T& operator[] (int i);

    int size();
    bool empty();
    void push_back(const T& newElement);
    void pop_back();
    void clear();
private:
    T* body;
    const int defaultBufferSize = 5000;
    int bufferSize;
    int vectorSize;
    void shrinkBuffer();
    void growBuffer();
};

template <typename T>
vector<T>::vector():
        vectorSize(0)
{
    bufferSize = defaultBufferSize;
    body = new T[bufferSize];
}

template <typename T>
vector<T>::vector(int defSize):
        vectorSize(0)
{
    bufferSize = defSize;
    body = new T[bufferSize];
}

template <typename T>
vector<T>::~vector()
{
    delete body;
}

template <typename T>
vector<T>::vector(const vector<T>& otherVector)
{
    bufferSize = otherVector.bufferSize;
    vectorSize = otherVector.vectorSize;
    body = new T[otherVector.vectorSize];

    for (int i = 0; i < vectorSize; ++i)
    {
        body[i] = otherVector.body[i];
    }
}

template <typename T>
vector<T>& vector<T>::operator=(const vector<T> &otherVector)
{
    if (&otherVector != this) {
        delete body;
        bufferSize = otherVector.bufferSize;
        vectorSize = otherVector.vectorSize;
        body = new T[otherVector.vectorSize];

        for (int i = 0; i < vectorSize; ++i) {
            body[i] = otherVector.body[i];
        }
    }
    return *this;
}

template <typename T>
T& vector<T>::operator[](int i)
{
    return body[i];
}

template <typename T>
int vector<T>::size()
{
    return vectorSize;
}

template <typename T>
bool vector<T>::empty()
{
    return vectorSize == 0;
}

template <typename T>
void vector<T>::push_back(const T& newElement)
{
    if (vectorSize == bufferSize)
    {
        growBuffer();
    }
    body[vectorSize++] = newElement;
}

template<typename T>
void vector<T>::pop_back()
{
    --vectorSize;
    if (4 * vectorSize <= bufferSize && bufferSize >= 2 * defaultBufferSize)
    {
        shrinkBuffer();
    }
}

template <typename T>
void vector<T>::clear()
{
    delete body;
    bufferSize = defaultBufferSize;
    vectorSize = 0;
    body = new T[bufferSize];
}

template <typename T>
void vector<T>::growBuffer()
{
    bufferSize *= 2;
    T* newBody = new T[bufferSize];
    for (int i = 0; i < vectorSize; ++i)
    {
        newBody[i] = body[i];
    }
    delete body;
    body = newBody;
}

template <typename T>
void vector<T>::shrinkBuffer()
{
    bufferSize /= 2;
    T* newBody = new T[bufferSize];

    for (int i = 0; i < bufferSize; ++i)
    {
        newBody[i] = body[i];
    }
    delete body;
    body = newBody;
}

template <typename T>
void swap(T& _t1, T& _t2)
{
    T tmp(_t1);
    _t1 = _t2;
    _t2 = tmp;
}

//HEAP
template <typename T>
class BinaryHeap
{
public:
    BinaryHeap();
    BinaryHeap(const vector<T>& elements);
    BinaryHeap(const BinaryHeap& otherHeap);
    ~BinaryHeap();

    T getMin();
    void eraseMin();
    void insert(const T& element);
    void clear();
    int heapSize() const;
private:
    vector<T> heapBody;
    int size;

    void siftUp(int i);
    void siftDown(int i);

};

template <typename T>
BinaryHeap<T>::BinaryHeap():
        heapBody(vector<T>()),
        size(0)
{
}

template<typename T>
BinaryHeap<T>::~BinaryHeap()
{
    heapBody.clear();
}

template<typename T>
BinaryHeap<T>::BinaryHeap(const vector<T> &elements)
{
    heapBody = elements;
    size = heapBody.size();
    for (int i = size - 1; i >= 0; --i)
    {
        siftDown(i);
    }
}

template<typename T>
BinaryHeap<T>::BinaryHeap(const BinaryHeap<T>& otherHeap) {
    heapBody = otherHeap.heapBody;
    size = otherHeap.size;
}

template<typename T>
void BinaryHeap<T>::siftDown(int i)
{
    while (2 * i + 1 < size)
    {
        int lc = 2 * i + 1;
        int rc = lc + 1;
        int toswp = lc;

        if (rc < size && heapBody[rc] < heapBody[toswp])
        {
            toswp = rc;
        }
        if (heapBody[toswp] < heapBody[i])
        {
            swap(heapBody[i], heapBody[toswp]);
            i = toswp;
        } else
        {
            break;
        }

    }
}

template<typename T>
void BinaryHeap<T>::siftUp(int i)
{
    while (i != 0 && heapBody[i] < heapBody[(i - 1) / 2])
    {
        swap(heapBody[i], heapBody[(i - 1) / 2]);
        i = (i - 1) / 2;
    }
}

template<typename T>
T BinaryHeap<T>::getMin()
{
    return heapBody[0];
}

template<typename T>
void BinaryHeap<T>::eraseMin()
{
    if (size != 0)
    {
        heapBody[0] = heapBody[size - 1];
    }
    heapBody.pop_back();

    --size;
    siftDown(0);
}

template<typename T>
void BinaryHeap<T>::insert(const T& element)
{
    heapBody.push_back(element);
    ++size;
    siftUp(size - 1);
}

template <typename T>
void BinaryHeap<T>::clear()
{
    heapBody.clear();
    size = 0;
}

template<typename T>
int BinaryHeap<T>::heapSize() const
{
    return size;
}

int main() {
    int n, k, elem;

    std::cin >> n >> k;
    BinaryHeap<int> heap;
    vector<int> result(k);

    for (int i = 0; i < k; ++i)
    {
        std::cin >> elem;
        heap.insert(-elem);
    }

    for (int i = k; i < n; ++i) {
        std::cin >> elem;
        heap.insert(-elem);
        heap.eraseMin();
    }

    for (int i = 0; i < k; ++i)
    {
        result[k - i - 1] = -heap.getMin();
        heap.eraseMin();
    }
    for (int i = 0; i < k; ++i)
    {
        std::cout << result[i] << " ";
    }
    return 0;
}